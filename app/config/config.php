<?php
    // DB Params
    define('DB_HOST', 'localhost');
    define('DB_USER', 'root');
    define('DB_PASS', 'root');
    define('DB_NAME', 'lebonmug');
    define('DB_PREFIX', '');

    // App Root
    define('APPROOT', dirname(dirname(__FILE__)));

    // URL Root
    define('URLROOT', 'http://lebonmug.valentinlegal.fr');

    // Site Name
    define('SITENAME', 'LeBonMug');

    // App Version
    define('APPVERSION', '1.0.0');
