<?php
    class User {
        private $db;

        public function __construct() {
            $this->db = new Database;
        }

        // Register User
        public function register($data) {
            $this->db->query('INSERT INTO ' . DB_PREFIX . 'users (email, password, pseudo, newsletterAlert) VALUES (:email, :password, :pseudo, :newsletterAlert)');
            // Bind values
            $this->db->bind(':email', $data['email']);
            $this->db->bind(':password', $data['pass']);
            $this->db->bind(':pseudo', $data['pseudo']);
            $this->db->bind(':newsletterAlert', $data['accept_newsletter']);

            if($this->db->execute()) {
                return true;
            } else {
                return false;
            }
        }

        // Complete User profile
        public function completeProfile($data) {
            $this->db->query('UPDATE ' . DB_PREFIX . 'users SET idCivilite = :civilite, nom = :nom, prenom = :prenom, adresse = :adresse, complement = :complement, ville = :ville, codePostal = :codePostal, avatar = :avatar WHERE id = :id');

            $this->db->bind(':id', $data['user']);
            $this->db->bind(':civilite', $data['civilite']);
            $this->db->bind(':nom', $data['nom']);
            $this->db->bind(':prenom', $data['prenom']);
            $this->db->bind(':adresse', $data['adresse']);
            $this->db->bind(':complement', $data['complement']);
            $this->db->bind(':ville', $data['ville']);
            $this->db->bind(':codePostal', $data['cp']);
            $this->db->bind(':avatar', $data['avatar']);

            if($this->db->execute()) {
                return true;
            } else {
                return false;
            }
        }

        // Update User
        public function updateUser($data) {
            $this->db->query('UPDATE ' . DB_PREFIX . 'users SET pseudo = :pseudo, email = :email, password = :password, idCivilite = :civilite, nom = :nom, prenom = :prenom, adresse = :adresse, complement = :complement, ville = :ville, codePostal = :codePostal, avatar = :avatar, emailAlert = :emailAlert, newsletterAlert = :newsletterAlert WHERE id = :id');

            $this->db->bind(':pseudo', $data['pseudo']);
            $this->db->bind(':email', $data['new_email']);
            $this->db->bind(':password', $data['new_password']);
            $this->db->bind(':id', $data['user']);
            $this->db->bind(':civilite', $data['civilite']);
            $this->db->bind(':nom', $data['nom']);
            $this->db->bind(':prenom', $data['prenom']);
            $this->db->bind(':adresse', $data['adresse']);
            $this->db->bind(':complement', $data['complement']);
            $this->db->bind(':ville', $data['ville']);
            $this->db->bind(':codePostal', $data['cp']);
            $this->db->bind(':avatar', $data['avatar']);
            $this->db->bind(':emailAlert', $data['email_alert']);
            $this->db->bind(':newsletterAlert', $data['newsletter_alert']);

            if($this->db->execute()) {
                return true;
            } else {
                return false;
            }
        }

        // Delete User (and Mug associated)
        public function deleteUser($id) {
            // Delete archived Mug
            $this->db->query('DELETE FROM ' . DB_PREFIX . 'archiveMugs WHERE idVendeur = :idVendeur');
            $this->db->bind(':idVendeur', $id);
            $this->db->execute();
            // Delete Commande
            $this->db->query('DELETE FROM ' . DB_PREFIX . 'commandes WHERE idClient = :idClient');
            $this->db->bind(':idClient', $id);
            $this->db->execute();
            // Delete LignePanier
            $this->db->query('DELETE FROM ' . DB_PREFIX . 'lignesPanierUser WHERE idUser = :idUser');
            $this->db->bind(':idUser', $id);
            $this->db->execute();
            // Delete Messages
            $this->db->query('DELETE FROM ' . DB_PREFIX . 'messages WHERE idAuteur = :idUser OR idDestinataire = :idUser');
            $this->db->bind(':idUser', $id);
            $this->db->execute();
            // Delete Mug
            $this->db->query('DELETE FROM ' . DB_PREFIX . 'mugs WHERE idVendeur = :idVendeur');
            $this->db->bind(':idVendeur', $id);
            $this->db->execute();
            // Delete User
            $this->db->query('DELETE FROM users WHERE id = :id');
            $this->db->bind(':id', $id);

            if($this->db->execute()) {
                return true;
            } else {
                return false;
            } 
        }

        // Login User
        public function login($email, $password) {
            $this->db->query('SELECT * FROM ' . DB_PREFIX . 'users WHERE email = :email');
            // Bind values
            $this->db->bind(':email', $email);

            $row = $this->db->single();
            $hashed_password = $row->password;
            if(password_verify($password, $hashed_password)) {
                return $row;
            } else {
                return false;
            }
        }

        // Find user by email
        public function findUserByEmail($email) {
            $this->db->query('SELECT * FROM ' . DB_PREFIX . 'users WHERE email = :email');
            // Bind values
            $this->db->bind(':email', $email);

            $row = $this->db->single();

            // Check row
            if($this->db->rowCount() > 0) {
                return true;
            } else {
                return false;
            }
        }

        public function findUserById($id) {
            $this->db->query('SELECT * FROM ' . DB_PREFIX . 'users WHERE id = :id');
            $this->db->bind(':id', $id);

            $row = $this->db->single();

            return $row;
        }

        public function getTypesLivraison() {
            $this->db->query('SELECT * FROM ' . DB_PREFIX . 'typesLivraison');
            $results = $this->db->resultSet();

            return $results;
        }

        public function addCommand($data) {
            // Create Rows Command
            foreach ($data['id_mugs'] as $mug => $idMug) {
                // $this->db->query('INSERT INTO lignesCommande (idCommande, idArticle) VALUES (:idCommande, :idArticle)');
                // $this->db->bind(':idCommande', $idMug);
                // $this->db->bind(':idArticle', $idArticle);
                // $this->db->execute();

                // Make mug with status Vendu
                $this->db->query('UPDATE ' . DB_PREFIX . 'mugs SET idStatut = 4 WHERE id = :id');
                $this->db->bind(':id', $idMug);
                $this->db->execute();
            }

            // Delete row from Panier
            foreach ($data['panier'] as $row) {
                $this->db->query('DELETE FROM ' . DB_PREFIX . 'lignesPanierUser WHERE id = :id');
                $this->db->bind(':id', $row->idLignePanier);
                $this->db->execute();
            }

            // Create Command
            $this->db->query('INSERT INTO ' . DB_PREFIX . 'commandes (idClient, idLivraison) VALUES (:idClient, :idLivraison)');
            $this->db->bind(':idClient', $data['id_client']);
            $this->db->bind(':idLivraison', $data['id_livraison']);

            if($this->db->execute()) {
                return true;
            } else {
                return false;
            }
        }
    }