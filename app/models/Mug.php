<?php
    class Mug {
        private $db;

        public function __construct() {
            $this->db = new Database;
        }

        public function addMug($data) {
            $this->db->query('INSERT INTO ' . DB_PREFIX . 'mugs (titre, description, prixHt, idVendeur, idEtat, idCouleur, idVolume, idType, photo1, photo2, photo3) VALUES (:titre, :description, :prixHt, :idVendeur, :idEtat, :idCouleur, :idVolume, :idType, :photo1, :photo2, :photo3)');

            $this->db->bind(':titre', $data['titre']);
            $this->db->bind(':description', $data['description']);
            $this->db->bind(':prixHt', $data['prix']);
            $this->db->bind(':idVendeur', $data['user']);
            $this->db->bind(':idEtat', $data['etat']);
            $this->db->bind(':idCouleur', $data['couleur']);
            $this->db->bind(':idVolume', $data['volume']);
            $this->db->bind(':idType', $data['type']);
            $this->db->bind(':photo1', $data['photo1']);
            $this->db->bind(':photo2', $data['photo2']);
            $this->db->bind(':photo3', $data['photo3']);

            if($this->db->execute()) {
                return true;
            } else {
                return false;
            } 
        }

        public function updateMug($data) {
            $this->db->query('UPDATE ' . DB_PREFIX . 'mugs SET titre = :titre, description = :description, prixHt = :prixHt, idEtat = :idEtat, idCouleur = :idCouleur, idVolume = :idVolume, idType = :idType, photo1 = :photo1, photo2 = :photo2, photo3 =:photo3, updatedAt = :updatedAt, idStatut = 1 WHERE id = :id');

            $this->db->bind(':id', $data['id']);
            $this->db->bind(':titre', $data['titre']);
            $this->db->bind(':description', $data['description']);
            $this->db->bind(':prixHt', $data['prix']);
            $this->db->bind(':idEtat', $data['etat']);
            $this->db->bind(':idCouleur', $data['couleur']);
            $this->db->bind(':idVolume', $data['volume']);
            $this->db->bind(':idType', $data['type']);
            $this->db->bind(':photo1', $data['photo1']);
            $this->db->bind(':photo2', $data['photo2']);
            $this->db->bind(':photo3', $data['photo3']);
            $this->db->bind(':updatedAt', $data['updated_at']);

            if($this->db->execute()) {
                return true;
            } else {
                return false;
            } 
        }
        
        public function deleteMug($id) {
            $this->db->query('DELETE FROM ' . DB_PREFIX . 'mugs WHERE id = :id');

            $this->db->bind(':id', $id);

            if($this->db->execute()) {
                return true;
            } else {
                return false;
            } 
        }

        public function getMugs() {
            $this->db->query('SELECT * FROM ' . DB_PREFIX . 'mugs ORDER BY createdAt DESC');
            $results = $this->db->resultSet();

            if($results) {
                $results = $this->formatPrice($results);
            }

            return $results;
        }

        public function getMugById($id) {
            $this->db->query('SELECT * FROM ' . DB_PREFIX . 'mugs WHERE id = :id');
            $this->db->bind(':id', $id);

            $row = $this->db->single();

            if($row) {
                $row = $this->formatPrice($row);
            }

            return $row;
        }



        public function getCouleurs() {
            $this->db->query('SELECT * FROM ' . DB_PREFIX . 'couleursMug ORDER BY libelle ASC');
            $results = $this->db->resultSet();

            return $results;
        }

        public function getCouleurById($id) {
            $this->db->query('SELECT * FROM ' . DB_PREFIX . 'couleursMug WHERE id = :id');
            $this->db->bind(':id', $id);

            $row = $this->db->single();

            return $row;
        }

        public function getTypes() {
            $this->db->query('SELECT * FROM ' . DB_PREFIX . 'typesMug ORDER BY libelle ASC');
            $results = $this->db->resultSet();

            return $results;
        }

        public function getTypeById($id) {
            $this->db->query('SELECT * FROM ' . DB_PREFIX . 'typesMug WHERE id = :id');
            $this->db->bind(':id', $id);

            $row = $this->db->single();

            return $row;
        }

        public function getVolumes() {
            $this->db->query('SELECT * FROM ' . DB_PREFIX . 'volumesMug ORDER BY volume ASC');
            $results = $this->db->resultSet();

            return $results;
        }

        public function getVolumeById($id) {
            $this->db->query('SELECT * FROM ' . DB_PREFIX . 'volumesMug WHERE id = :id');
            $this->db->bind(':id', $id);

            $row = $this->db->single();

            return $row;
        }

        public function getEtats() {
            $this->db->query('SELECT * FROM ' . DB_PREFIX . 'etatsMug');
            $results = $this->db->resultSet();

            return $results;
        }

        public function getEtatById($id) {
            $this->db->query('SELECT * FROM ' . DB_PREFIX . 'etatsMug WHERE id = :id');
            $this->db->bind(':id', $id);

            $row = $this->db->single();

            return $row;
        }

        public function getMugsByProperty($prop) {
            switch ($prop) {
                case 'plus_recents':
                    $this->db->query('SELECT * FROM ' . DB_PREFIX . 'mugs ORDER BY createdAt DESC');
                    break;
                case 'plus_anciens':
                    $this->db->query('SELECT * FROM ' . DB_PREFIX . 'mugs ORDER BY createdAt ASC');
                    break;
                case 'prix_croissants':
                    $this->db->query('SELECT * FROM ' . DB_PREFIX . 'mugs ORDER BY prixHt ASC');
                    break;
                case 'prix_decroissants':
                    $this->db->query('SELECT * FROM ' . DB_PREFIX . 'mugs ORDER BY prixHt DESC');
                    break;
            }

            $results = $this->db->resultSet();

            if($results) {
                $results = $this->formatPrice($results);
            }

            return $results;
        }

        public function getMugsWithFilters($couleur, $type, $volume, $etat, $prixMin, $prixMax, $date) {
            

            if($couleur != '0' && $type != '0' && $volume == '0' && $etat == '0') {
                
                $this->db->query('SELECT * FROM ' . DB_PREFIX . 'mugs
                                WHERE idCouleur = :idCouleur
                                ORDER BY createdAt DESC
                                ');
                $this->db->bind(':idCouleur', $couleur);
            }

            $results = $this->db->resultSet();

            if($results) {
                $results = $this->formatPrice($results);
            }

            return $results;
        }

        public function getMugsByUserId($id, $order = '') {
            if (empty($order)) {
                $this->db->query('SELECT * FROM ' . DB_PREFIX . 'mugs WHERE idVendeur = :id ORDER BY createdAt DESC');
            } else {
                switch ($order) {
                    case 'plus_recents':
                        $this->db->query('SELECT * FROM ' . DB_PREFIX . 'mugs WHERE idVendeur = :id ORDER BY createdAt DESC');
                        break;
                    case 'plus_anciens':
                        $this->db->query('SELECT * FROM ' . DB_PREFIX . 'mugs WHERE idVendeur = :id ORDER BY createdAt ASC');
                        break;
                    case 'prix_croissants':
                        $this->db->query('SELECT * FROM ' . DB_PREFIX . 'mugs WHERE idVendeur = :id ORDER BY prixHt ASC');
                        break;
                    case 'prix_decroissants':
                        $this->db->query('SELECT * FROM ' . DB_PREFIX . 'mugs WHERE idVendeur = :id ORDER BY prixHt DESC');
                        break;
                }
            }

            $this->db->bind(':id', $id);

            $results = $this->db->resultSet();

            if($results) {
                $results = $this->formatPrice($results);
            }

            return $results;
        }

        public function addMugToPanier($userId, $mugId) {
            $this->db->query('UPDATE ' . DB_PREFIX . 'mugs SET idStatut = 5 WHERE id = :id');
            $this->db->bind(':id', $mugId);
            $this->db->execute();

            $this->db->query('INSERT INTO ' . DB_PREFIX . 'lignesPanierUser (idUser, idArticle) VALUES (:idUser, :idArticle)');
            $this->db->bind(':idUser', $userId);
            $this->db->bind(':idArticle', $mugId);

            if($this->db->execute()) {
                return true;
            } else {
                return false;
            } 
        }

        public function getRowPanier($id) {
            $this->db->query('SELECT idUser FROM ' . DB_PREFIX . 'lignesPanierUser WHERE id = :id');
            $this->db->bind(':id', $id);
            $row = $this->db->single();

            return $row;
        }

        public function getMugsInPanier($userId) {
            $this->db->query('SELECT ' . DB_PREFIX . 'mugs.id, ' . DB_PREFIX . 'mugs.titre, ' . DB_PREFIX . 'mugs.prixHt, ' . DB_PREFIX . 'mugs.photo1, ' . DB_PREFIX . 'users.id AS idVendeur, ' . DB_PREFIX . 'users.pseudo AS vendeur, ' . DB_PREFIX . 'lignesPanierUser.id as idLignePanier, ' . DB_PREFIX . 'lignesPanierUser.createdAt
                            FROM ' . DB_PREFIX . 'lignesPanierUser
                            JOIN ' . DB_PREFIX . 'mugs ON ' . DB_PREFIX . 'lignesPanierUser.idArticle = ' . DB_PREFIX . 'mugs.id
                            JOIN ' . DB_PREFIX . 'users ON ' . DB_PREFIX . 'mugs.idVendeur = ' . DB_PREFIX . 'users.id
                            WHERE idUser = :idUser 
                            ORDER BY createdAt DESC
                            ');

            $this->db->bind(':idUser', $userId);

            $results = $this->db->resultSet();

            return $results;
        }

        public function deleteRowPanier($idRowPanier) {
            $this->db->query('SELECT idArticle FROM ' . DB_PREFIX . 'lignesPanierUser WHERE id = :id');
            $this->db->bind(':id', $idRowPanier);
            $mugId = $this->db->single();

            $this->db->query('UPDATE ' . DB_PREFIX . 'mugs SET idStatut = 1 WHERE id = :id');
            $this->db->bind(':id', $mugId->idArticle);
            $this->db->execute();

            $this->db->query('DELETE FROM ' . DB_PREFIX . 'lignesPanierUser WHERE id = :id');
            $this->db->bind(':id', $idRowPanier);

            if($this->db->execute()) {
                return true;
            } else {
                return false;
            } 
        }



        
        /* ===== Usefull Functions ===== */

        public function formatPrice($mugs) {
            if(is_array($mugs)) {
                foreach ($mugs as $mug) {
                    // Format HT Price and add TTC Price
                    $mug->prixTtc = number_format($mug->prixHt + $mug->prixHt * 0.20, 2, ',', ' ');
                    $mug->prixHt = number_format($mug->prixHt, 2, ',', ' ');
                }
            } else {
                // Format HT Price and add TTC Price
                $mugs->prixTtc = number_format($mugs->prixHt + $mugs->prixHt * 0.20, 2, ',', ' ');
                $mugs->prixHt = number_format($mugs->prixHt, 2, ',', ' ');
            }
            

            return $mugs;
        }
    }