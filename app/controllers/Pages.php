<?php
    class Pages extends Controller {
        public function __construct() {
            $this->mugModel = $this->model('Mug');

            if(isset($_SESSION['user_id']) && empty($_SESSION['user_civilite'])) {
                redirect('users/finalisation');
            }
        }

        public function index() {
            $mugs =  $this->mugModel->getMugs();
            // Select Data
            $couleurs =  $this->mugModel->getCouleurs();
            $types =  $this->mugModel->getTypes();
            $volumes =  $this->mugModel->getVolumes();
            $etats =  $this->mugModel->getEtats();

            if($_SERVER['REQUEST_METHOD'] == 'POST') {
                // Sanitize POST data
                $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
                
                // Process for Sort
                if(isset($_POST['prix_min'])) {
                    $data = [
                        'mugs' => $mugs,
                        'selected_tri' => 'plus_recents',
                        // Props
                        'couleur' => $_POST['couleur'],
                        'type' => $_POST['type'],
                        'volume' => $_POST['volume'],
                        'etat' => $_POST['etat'],
                        'prix_min' => check_input($_POST['prix_min']),
                        'prix_max' => check_input($_POST['prix_max']),
                        'date' => check_input($_POST['date']),
                        // Error
                        'prix_min_err' => '',
                        'prix_max_err' => '',
                        'date_err' => '',
                        // Select Data
                        'select_couleurs' => $couleurs,
                        'select_types' => $types,
                        'select_volumes' => $volumes,
                        'select_etats' => $etats,
                    ];

                    $data['prix_min'] = str_replace(' ', '', $data['prix_min']);
                    $data['prix_max'] = str_replace(' ', '', $data['prix_max']);

                    // Validate Prix Min
                    if(empty($data['prix_min'])) {
                        $data['prix_min_err'] = 'Veuillez entrer un prix minimum.';
                    } elseif(!preg_match('/^\d+$/', $data['prix_min'])) {
                        $data['prix_min_err'] = 'Veuillez entrer un chiffre entier (ex : 1234).';
                    }

                    // Validate Prix Max
                    if(empty($data['prix_max'])) {
                        $data['prix_max_err'] = 'Veuillez entrer un prix maximum.';
                    } elseif(!preg_match('/^\d+$/', $data['prix_min'])) {
                        $data['prix_max_err'] = 'Veuillez entrer un chiffre entier (ex : 1234).';
                    } elseif(intval($data['prix_min']) >= intval($data['prix_max'])) {
                        $data['prix_max_err'] = 'Le prix maximum doit être plus grand que le prix minimum.';
                    }

                    // Validate Date
                    if(empty($data['date'])) {
                        $data['date_err'] = 'Veuillez entrer une date.';
                    } elseif($data['date'] > date('Y-m-d')) {
                        $data['date_err'] = 'La date doit être inférieur ou égale à celle d\'aujoud\'hui.';
                    }

                    if(empty($data['prix_min_err']) && empty($data['prix_max_err']) && empty($data['date_err'])) {
                        $results = $this->mugModel->getMugsWithFilters($_POST['couleur'], $_POST['type'], $_POST['volume'], $_POST['etat'], $_POST['prix_min'], $_POST['prix_max'], $_POST['date']);
                        $data['res_search'] = $results;
                    }

                    $this->view('pages/index', $data);
                }

                // Process for Filter
                if(isset($_POST['tri'])) {
                    $mugs = $this->mugModel->getMugsByProperty($_POST['tri']);
                    $data = [
                        'mugs' => $mugs,
                        'selected_tri' => $_POST['tri'],
                        // Props
                        'couleur' => '',
                        'type' => '',
                        'volume' => '',
                        'etat' => '',
                        'prix_min' => '',
                        'prix_max' => '',
                        'date' => '',
                        // Error
                        'prix_min_err' => '',
                        'prix_max_err' => '',
                        'date_err' => '',
                        // Select Data
                        'select_couleurs' => $couleurs,
                        'select_types' => $types,
                        'select_volumes' => $volumes,
                        'select_etats' => $etats,
                    ];
                    $this->view('pages/index', $data);
                }
            } else {
                $data = [
                    'mugs' => $mugs,
                    'selected_tri' => 'plus_recents',
                    // Props
                    'couleur' => '',
                    'type' => '',
                    'volume' => '',
                    'etat' => '',
                    'prix_min' => '',
                    'prix_max' => '',
                    'date' => '',
                    // Error
                    'prix_min_err' => '',
                    'prix_max_err' => '',
                    'date_err' => '',
                    // Select Data
                    'select_couleurs' => $couleurs,
                    'select_types' => $types,
                    'select_volumes' => $volumes,
                    'select_etats' => $etats,
                ];

                $this->view('pages/index', $data);
            }
        }
    }