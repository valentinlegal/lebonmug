<?php
    class Mugs extends Controller {
        public function __construct() {
            $this->mugModel = $this->model('Mug');
            $this->userModel = $this->model('User');

            if(isset($_SESSION['user_id']) && empty($_SESSION['user_civilite'])) {
                redirect('users/finalisation');
            }
        }

        public function index($id = null) {
            if(!$id) {
                redirect('');
            }

            $mug = $this->mugModel->getMugById($id);
            $mug->couleur = $this->mugModel->getCouleurById($mug->idCouleur)->libelle;
            $mug->type = $this->mugModel->getTypeById($mug->idType)->libelle;
            $mug->volume = $this->mugModel->getVolumeById($mug->idVolume)->volume;
            $mug->etat = $this->mugModel->getEtatById($mug->idEtat)->libelle;
            $user = $this->userModel->findUserById($mug->idVendeur);

            if($_SERVER['REQUEST_METHOD'] == 'POST') {
                if(isset($_POST['add_panier'])) {
                    // User need to be logged
                    if(!isLoggedIn()) {
                        redirect('users/connexion');
                    } else {
                        if($this->mugModel->addMugToPanier($_SESSION['user_id'], $mug->id)) {
                            flash('mug_message', 'Votre mug a bien été ajouté à votre panier !');
                            redirect('users/panier');
                        } else {
                            die("Something went wrong");
                        }
                    }
                }
            } else {
                $data = [
                    'mug' => $mug,
                    'user' => $user
                ];
    
                $this->view('mugs/index', $data);
            }

        }

        public function add() {
            // User need to be logged
            if(!isLoggedIn()) {
                redirect('users/connexion');
            }

            // Select Data
            $couleurs =  $this->mugModel->getCouleurs();
            $types =  $this->mugModel->getTypes();
            $volumes =  $this->mugModel->getVolumes();
            $etats =  $this->mugModel->getEtats();

            // Check for POST Request
            if($_SERVER['REQUEST_METHOD'] == 'POST') {
                // Sanitize POST array
                $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
                $data = [
                    'user' => $_SESSION['user_id'],
                    'titre' => check_input($_POST['titre']),
                    'description' => check_input($_POST['description']),
                    'prix' => check_input($_POST['prix']),
                    'couleur' => (isset($_POST['couleur'])) ? $_POST['couleur'] : '',
                    'type' => (isset($_POST['type'])) ? $_POST['type'] : '',
                    'volume' => (isset($_POST['volume'])) ? $_POST['volume'] : '',
                    'etat' => (isset($_POST['etat'])) ? $_POST['etat'] : '',
                    'photo1' => !empty($_FILES['photo1']) ? check_input($_FILES['photo1']['name']) : null,
                    'photo2' => !empty($_FILES['photo2']) ? check_input($_FILES['photo2']['name']) : null,
                    'photo3' => !empty($_FILES['photo3']) ? check_input($_FILES['photo3']['name']) : null,
                    // Error messages
                    'titre_err' => '',
                    'description_err' => '',
                    'prix_err' => '',
                    'couleur_err' => '',
                    'type_err' => '',
                    'volume_err' => '',
                    'etat_err' => '',
                    'photo1_err' => '',
                    'photo2_err' => '',
                    'photo3_err' => '',
                    // Select
                    'select_couleurs' => $couleurs,
                    'select_types' => $types,
                    'select_volumes' => $volumes,
                    'select_etats' => $etats,
                ];

                // Validate Titre
                if(empty($data['titre'])) {
                    $data['titre_err'] = 'Veuillez entrer un titre.';
                }

                // Validate Description
                if(empty($data['description'])) {
                    $data['description_err'] = 'Veuillez entrer une description.';
                }

                // Validate Prix
                if(empty($data['prix'])) {
                    $data['prix_err'] = 'Veuillez entrer un prix.';
                } elseif (!preg_match('/^(\$)?([1-9]{1}[0-9]{0,2})(\ \d{3})*(\,\d{2})?$|^(\$)?([1-9]{1}[0-9]{0,2})(\d{3})*(\,\d{2})?$|^(0)?(\,\d{2})?$|^(\$0)?(\,\d{2})?$|^(\$\,)(\d{2})?$/', $data['prix'])) {
                    $data['prix_err'] = 'Veuillez entrer un prix avec le bon format (ex : 12 / 12,34 / 1234 / 1234,56 / 1 234 / 1 234,56 / etc).';
                } else {
                    $data['prix'] = str_replace(',', '.', $data['prix']);
                    $data['prix'] = str_replace(' ', '', $data['prix']);
                }

                // Validate Couleur
                if(empty($data['couleur'])) {
                    $data['couleur_err'] = 'Veuillez sélectionner une couleur.';
                }

                // Validate Type
                if(empty($data['type'])) {
                    $data['type_err'] = 'Veuillez sélectionner un type.';
                }

                // Validate Volume
                if(empty($data['volume'])) {
                    $data['volume_err'] = 'Veuillez sélectionner un volume.';
                }

                // Validate Etat
                if(empty($data['etat'])) {
                    $data['etat_err'] = 'Veuillez sélectionner un état.';
                }

                // Validate Photo 1
                if (!empty($data['photo1'])) {
                    $res = upload_image_helper($_FILES['photo1'], '/mugs');
                    $res[0] ? $data['photo1'] = $res[1] : $data['photo1_err'] = $res[1];
                } else {
                    $data['photo1_err'] = 'Veuillez importer au moins une photo.';
                }

                // Validate Photo 2
                if (!empty($data['photo2'])) {
                    $res = upload_image_helper($_FILES['photo2'], '/mugs');
                    $res[0] ? $data['photo2'] = $res[1] : $data['photo2_err'] = $res[1];
                } else {
                    $data['photo2'] = null;
                }

                // Validate Photo 3
                if (!empty($data['photo3'])) {
                    $res = upload_image_helper($_FILES['photo3'], '/mugs');
                    $res[0] ? $data['photo3'] = $res[1] : $data['photo3_err'] = $res[1];
                } else {
                    $data['photo3'] = null;
                }

                // Make sure errors are empty
                if(empty($data['titre_err']) && empty($data['description_err']) && empty($data['prix_err']) && empty($data['couleur_err']) && empty($data['type_err']) && empty($data['volume_err']) && empty($data['etat_err']) && empty($data['photo1_err']) && empty($data['photo2_err']) && empty($data['photo3_err'])) {
                    // Register Mug
                    if($this->mugModel->addMug($data)) {
                        flash('mug_message', 'Votre annonce a bien été enregistrée !');
                        // flash('mug_message', 'Votre annonce a bien été enregistrée ! Elle devra être vérifiée par un modérateur avant de devenir publique.');
                        redirect('');
                    } else {
                        die('Something went wrong');
                    }
                } else {
                    // Load view with errors
                    flash('user_message', 'Ajout impossible : veuillez vérifier les erreurs trouvées.', 'alert alert-danger');
                    $this->view('mugs/add', $data);
                }
            } else {
                // Init data
                $data = [
                    'titre' => '',
                    'description' => '',
                    'prix' => '',
                    'couleur' => '',
                    'type' => '',
                    'volume' => '',
                    'etat' => '',
                    'photo1' => '',
                    'photo2' => '',
                    'photo3' => '',
                    // Error messages
                    'titre_err' => '',
                    'description_err' => '',
                    'prix_err' => '',
                    'couleur_err' => '',
                    'type_err' => '',
                    'volume_err' => '',
                    'etat_err' => '',
                    'photo1_err' => '',
                    // Select
                    'select_couleurs' => $couleurs,
                    'select_types' => $types,
                    'select_volumes' => $volumes,
                    'select_etats' => $etats,
                ];

                $this->view('mugs/add', $data);
            }
        }

        public function edit($id = null) {
            if ($id) {
                // User need to be logged
                if(!isLoggedIn()) {
                    redirect('users/connexion');
                }

                // Get existing mug from model
                $mug = $this->mugModel->getMugById($id);

                if($mug->idStatut == '2' || $mug->idStatut == '4') {
                    redirect('');
                }

                // Select Data
                $couleurs =  $this->mugModel->getCouleurs();
                $types =  $this->mugModel->getTypes();
                $volumes =  $this->mugModel->getVolumes();
                $etats =  $this->mugModel->getEtats();

                // Check for POST Request
                if($_SERVER['REQUEST_METHOD'] == 'POST') {
                    // Sanitize POST array
                    $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);

                    $data = [
                        'id' => $id,
                        'titre' => check_input($_POST['titre']),
                        'description' => check_input($_POST['description']),
                        'prix' => check_input($_POST['prix']),
                        'couleur' => (isset($_POST['couleur'])) ? $_POST['couleur'] : '',
                        'type' => (isset($_POST['type'])) ? $_POST['type'] : '',
                        'volume' => (isset($_POST['volume'])) ? $_POST['volume'] : '',
                        'etat' => (isset($_POST['etat'])) ? $_POST['etat'] : '',
                        'photo1' => !empty($_FILES['photo1']) ? check_input($_FILES['photo1']['name']) : null,
                        'photo2' => !empty($_FILES['photo2']) ? check_input($_FILES['photo2']['name']) : null,
                        'photo3' => !empty($_FILES['photo3']) ? check_input($_FILES['photo3']['name']) : null,
                        'updated_at' => date('Y-m-d H:i:s'),
                        // Error messages
                        'titre_err' => '',
                        'description_err' => '',
                        'prix_err' => '',
                        'couleur_err' => '',
                        'type_err' => '',
                        'volume_err' => '',
                        'etat_err' => '',
                        'photo1_err' => '',
                        'photo2_err' => '',
                        'photo3_err' => '',
                        // Select
                        'select_couleurs' => $couleurs,
                        'select_types' => $types,
                        'select_volumes' => $volumes,
                        'select_etats' => $etats,
                    ];

                    // Validate Titre
                    if(empty($data['titre'])) {
                        $data['titre_err'] = 'Veuillez entrer un titre.';
                    }

                    // Validate Description
                    if(empty($data['description'])) {
                        $data['description_err'] = 'Veuillez entrer une description.';
                    }

                    // Validate Prix
                    if(empty($data['prix'])) {
                        $data['prix_err'] = 'Veuillez entrer un prix.';
                    } elseif (!preg_match('/^(\$)?([1-9]{1}[0-9]{0,2})(\ \d{3})*(\,\d{2})?$|^(\$)?([1-9]{1}[0-9]{0,2})(\d{3})*(\,\d{2})?$|^(0)?(\,\d{2})?$|^(\$0)?(\,\d{2})?$|^(\$\,)(\d{2})?$/', $data['prix'])) {
                        $data['prix_err'] = 'Veuillez entrer un prix avec le bon format (ex : 12 / 12,34 / 1234 / 1234,56 / 1 234 / 1 234,56 / etc).';
                    } else {
                        $data['prix'] = str_replace(',', '.', $data['prix']);
                        $data['prix'] = str_replace(' ', '', $data['prix']);
                    }

                    // Validate Couleur
                    if(empty($data['couleur'])) {
                        $data['couleur_err'] = 'Veuillez sélectionner une couleur.';
                    }

                    // Validate Type
                    if(empty($data['type'])) {
                        $data['type_err'] = 'Veuillez sélectionner un type.';
                    }

                    // Validate Volume
                    if(empty($data['volume'])) {
                        $data['volume_err'] = 'Veuillez sélectionner un volume.';
                    }

                    // Validate Etat
                    if(empty($data['etat'])) {
                        $data['etat_err'] = 'Veuillez sélectionner un état.';
                    }

                    // Validate Photo 1
                    if (!empty($data['photo1'])) {
                        $res = upload_image_helper($_FILES['photo1'], '/mugs');
                            $res[0] ? $data['photo1'] = $res[1] : $data['photo1_err'] = $res[1];
                    } else {
                        $data['photo1'] = $mug->photo1;
                    }

                    // Validate Photo 2
                    if (!empty($data['photo2'])) {
                        $res = upload_image_helper($_FILES['photo2'], '/mugs');
                            $res[0] ? $data['photo2'] = $res[1] : $data['photo2_err'] = $res[1];
                    } else {
                        $data['photo2'] = $mug->photo2;
                    }

                    // Validate Photo 3
                    if (!empty($data['photo3'])) {
                        $res = upload_image_helper($_FILES['photo3'], '/mugs');
                            $res[0] ? $data['photo3'] = $res[1] : $data['photo3_err'] = $res[1];
                    } else {
                        $data['photo3'] = $mug->photo3;
                    }

                    // Make sure errors are empty
                    if(empty($data['titre_err']) && empty($data['description_err']) && empty($data['prix_err']) && empty($data['couleur_err']) && empty($data['type_err']) && empty($data['volume_err']) && empty($data['etat_err']) && empty($data['photo1_err']) && empty($data['photo2_err']) && empty($data['photo3_err'])) {
                        // Register Mug
                        if($this->mugModel->updateMug($data)) {
                            flash('mug_message', 'Votre annonce a bien été modifiée !');
                            // flash('mug_message', 'Votre annonce a bien été modifiée ! Elle devra être vérifiée par un modérateur avant de devenir publique.');
                            redirect('');
                        } else {
                            die('Something went wrong');
                        }
                    } else {
                        // Load view with errors
                        flash('user_message', 'Mis à jour impossible : veuillez vérifier les erreurs trouvées.', 'alert alert-danger');
                        $this->view('mugs/edit', $data);
                    }
                } else {
                    // Check for owner
                    if($mug->idVendeur != $_SESSION['user_id']) {
                        redirect('');
                    }

                    // Init data
                    $data = [
                        'id' => $id,
                        'statut' => $mug->idStatut,
                        'titre' => $mug->titre,
                        'description' => $mug->description,
                        'prix' => $mug->prixHt,
                        'couleur' => $mug->idCouleur,
                        'type' => $mug->idType,
                        'volume' => $mug->idVolume,
                        'etat' => $mug->idEtat,
                        'photo1' =>  $mug->photo1,
                        'photo2' => $mug->photo2,
                        'photo3' => $mug->photo3,
                        // Error messages
                        'titre_err' => '',
                        'description_err' => '',
                        'prix_err' => '',
                        'couleur_err' => '',
                        'type_err' => '',
                        'volume_err' => '',
                        'etat_err' => '',
                        'photo1_err' => '',
                        'photo2_err' => '',
                        'photo3_err' => '',
                        // Select
                        'select_couleurs' => $couleurs,
                        'select_types' => $types,
                        'select_volumes' => $volumes,
                        'select_etats' => $etats,
                    ];

                    $this->view('mugs/edit', $data);
                }
            } else {
                redirect('');
            }
        }

        public function delete($id = null) {
            if ($id) {
                // Get existing mug from model
                $mug = $this->mugModel->getMugById($id);

                // Check if the mug exists
                if($mug) {
                    if($_SERVER['REQUEST_METHOD'] == 'POST') {
                        // Check for owner
                        if($mug->idVendeur != $_SESSION['user_id']) {
                            redirect('');
                        }
        
                        if($this->mugModel->deleteMug($id)) {
                            flash('mug_message', 'Votre annonce a bien été supprimée !');
                            redirect('');
                        } else {
                            die('Something went wrong');
                        }
                    } else {
                        redirect('');
                    }
                } else {
                    redirect('');
                }
            } else {
                redirect('');
            }
        }
    }