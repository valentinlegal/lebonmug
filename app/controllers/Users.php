<?php
    class Users extends Controller {
        public function __construct() {
            $this->userModel = $this->model('User');
            $this->mugModel = $this->model('Mug');
        }

        public function inscription() {
            // Check for POST
            if($_SERVER['REQUEST_METHOD'] == 'POST') {
                // Process form

                // Sanitize POST data
                $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
                
                // Init data
                $data = [
                    'pseudo' => check_input($_POST['pseudoInput']),
                    'email' => check_input($_POST['emailInput']),
                    'pass' => check_input($_POST['passwordInput1']),
                    'confirm_pass' => check_input($_POST['passwordInput2']),
                    'accept_newsletter' => (isset($_POST['newsletterCheck'])) ? '1' : '0',
                    'accept_conditions' => (isset($_POST['conditionsCheck'])) ? '1' : '0',
                    // Error messages
                    'pseudo_err' => '',
                    'email_err' => '',
                    'pass_err' => '',
                    'confirm_pass_err' => '',
                    'accept_conditions_err' => ''
                ];

                // Validate Pseudo
                if(empty($data['pseudo'])) {
                    $data['pseudo_err'] = 'Veuillez entrer votre pseudo.';
                }

                // Validate Email
                if(empty($data['email'])) {
                    $data['email_err'] = 'Veuillez entrer votre e-mail.';
                } elseif ($this->userModel->findUserByEmail($data['email'])) {
                    $data['email_err'] = 'Cet e-mail est déjà utilisé, veuillez en choisir un autre.';
                }

                // Validate Password
                if(empty($data['pass'])) {
                    $data['pass_err'] = 'Veuillez entrer votre mot de passe.';
                } elseif (strlen($data['pass']) < 10) {
                    $data['pass_err'] = 'Le mot de passe doit contenir 10 caractères minimum.';
                }

                // Validate Confirm Password
                if(empty($data['confirm_pass'])) {
                    $data['confirm_pass_err'] = 'Veuillez entrer la confirmation de votre mot de passe.';
                } elseif ($data['confirm_pass'] != $data['pass']) {
                    $data['confirm_pass_err'] = 'Le mot de passe ne correspond pas au précédemment.';
                }

                // Validate Conditions
                if($data['accept_conditions'] == '0') {
                    $data['accept_conditions_err'] = 'Veuillez accepter les Conditions du site.';
                }

                // Make sure errors are empty
                if(empty($data['pseudo_err']) && empty($data['email_err']) && empty($data['pass_err']) && empty($data['confirm_pass_err']) && empty($data['accept_conditions_err'])) {
                    // Validated
                    
                    // Hash Password
                    $data['pass'] = password_hash($data['pass'], PASSWORD_DEFAULT);

                    // Register User
                    if($this->userModel->register($data)) {
                        flash('user_message', 'Vous êtes bien inscrit ! Vous pouvez désormais vous connecter !');
                        redirect('users/connexion');
                    } else {
                        die('Something went wrong');
                    }
                } else {
                    // Load view with errors
                    $this->view('/users/inscription', $data);
                }


            } else {
                if (isset($_SESSION['user_id'])) {
                    // If logged user
                    redirect('');
                } else {
                    // Init data
                    $data = [
                        'pseudo' => '',
                        'email' => '',
                        'pass' => '',
                        'confirm_pass' => '',
                        'accept_newsletter' => false,
                        'accept_conditions' => false,
                        // Error messages
                        'pseudo_err' => '',
                        'email_err' => '',
                        'pass_err' => '',
                        'confirm_pass_err' => '',
                        'accept_conditions_err' => ''
                    ];
    
                    // Load view
                    $this->view('users/inscription', $data);
                }
            }
        }

        public function connexion() {
            // Check for POST
            if($_SERVER['REQUEST_METHOD'] == 'POST') {
                // Process form

                // Sanitize POST data
                $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
                
                // Init data
                $data = [
                    'email' => check_input($_POST['emailInput']),
                    'pass' => check_input($_POST['passwordInput']),
                    // Error messages
                    'email_err' => '',
                    'pass_err' => '',
                ];

                // Validate Email
                if(empty($data['email'])) {
                    $data['email_err'] = 'Veuillez entrer votre e-mail.';
                }

                // Validate Password
                if(empty($data['pass'])) {
                    $data['pass_err'] = 'Veuillez entrer votre mot de passe.';
                }

                // Check for user / email
                if(!$this->userModel->findUserByEmail($data['email'])) {
                    // No user found
                    $data['email_err'] = 'Aucun utilisateur enregistré avec cet email.';
                }

                // Make sure errors are empty
                if(empty($data['email_err']) && empty($data['pass_err'])) {
                    // Validated
                    // Check and set logged in user
                    $loggedInUser = $this->userModel->login($data['email'], $data['pass']);

                    if($loggedInUser) {
                        // Create Session
                        $this->createUserSession($loggedInUser);
                    } else {
                        $data['pass_err'] = 'Le mot de passe est incorrect';
                        $this->view('users/connexion', $data);
                    }
                } else {
                    // Load view with errors
                    $this->view('users/connexion', $data);
                }


            } else {
                if (isset($_SESSION['user_id'])) {
                    // If logged user
                    redirect('');
                } else {
                    // Init data
                    $data = [
                        'email' => '',
                        'pass' => '',
                        // Error messages
                        'email_err' => '',
                        'pass_err' => ''
                    ];
    
                    // Load view
                    $this->view('users/connexion', $data);
                }
            }
        }

        public function createUserSession($user) {
            $_SESSION['user_id'] = $user->id;
            $_SESSION['user_role'] = $user->idRole;
            $_SESSION['user_civilite'] = $user->idCivilite;

            if(empty($_SESSION['user_civilite'])) {
                redirect('users/finalisation');
            } else {
                redirect('');
            }
        }

        public function deconnexion() {
            unset($_SESSION['user_id']);
            unset($_SESSION['user_role']);
            unset($_SESSION['user_civilite']);
            session_destroy();
            redirect('users/connexion');
        }

        public function finalisation() {
            if(isset($_SESSION['user_id']) && empty($_SESSION['user_civilite'])) {
                // Check for POST
                if($_SERVER['REQUEST_METHOD'] == 'POST') {
                    // Process form

                    // Sanitize POST data
                    $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
                    
                    // Init data
                    $data = [
                        'user' => $_SESSION['user_id'],
                        'civilite' => isset($_POST['civilite']) ? $_POST['civilite'] : '',
                        'nom' => check_input($_POST['nom']),
                        'prenom' => check_input($_POST['prenom']),
                        'adresse' => check_input($_POST['adresse']),
                        'complement' => !empty($_POST['complement']) ? check_input($_POST['complement']) : null,
                        'ville' => check_input($_POST['ville']),
                        'cp' => check_input(str_replace(' ', '', $_POST['cp'])),
                        'avatar' => !empty($_FILES['avatar']) ? check_input($_FILES['avatar']['name']) : null,
                        // Error messages
                        'civilite_err' => '',
                        'nom_err' => '',
                        'prenom_err' => '',
                        'adresse_err' => '',
                        'ville_err' => '',
                        'cp_err' => ''
                    ];

                    // Validate Civilite
                    if(empty($data['civilite'])) {
                        $data['civilite_err'] = 'Veuillez sélectionner votre civilité.';
                    }

                    // Validate Nom
                    if(empty($data['nom'])) {
                        $data['nom_err'] = 'Veuillez entrer votre nom.';
                    }

                    // Validate Prenom
                    if(empty($data['prenom'])) {
                        $data['prenom_err'] = 'Veuillez entrer votre prénom.';
                    }

                    // Validate Adresse
                    if(empty($data['adresse'])) {
                        $data['adresse_err'] = 'Veuillez entrer votre adresse.';
                    }

                    // Validate Ville
                    if(empty($data['ville'])) {
                        $data['ville_err'] = 'Veuillez entrer votre ville.';
                    }

                    // Validate Code Postal
                    if(empty($data['cp'])) {
                        $data['cp_err'] = 'Veuillez entrer votre code postal.';
                    } elseif(!preg_match('/^[0-9]{5,5}$/' , $data['cp'] )) {
                        $data['cp_err'] = 'Le code postal doit contenir uniquement 5 chiffres (ex: 75000).';
                    }

                    // Check for Avatar
                    if(empty($data['avatar']) && !empty($data['civilite'])) {
                        if ($data['civilite'] == '1') {
                            // Woman avatar
                            $data['avatar'] = 'avatar_default_woman.png';
                        } else {
                            // Man avatar
                            $data['avatar'] = 'avatar_default_man.png';
                        }
                    } elseif(!empty($data['avatar'])) {
                        $res = upload_image_helper($_FILES['avatar'], '/avatars');
                        $res[0] ? $data['avatar'] = $res[1] : $data['avatar_err'] = $res[1];
                    }

                    // Make sure errors are empty
                    if(empty($data['civilite_err']) && empty($data['nom_err']) && empty($data['prenom_err']) && empty($data['adresse_err']) && empty($data['ville_err']) && empty($data['cp_err']) && empty($data['avatar_err'])) {
                        // Validated

                        // Complete User profile
                        if($this->userModel->completeProfile($data)) {
                            unset($_SESSION['user_civilite']);
                            $_SESSION['user_civilite'] = $data['civilite'];

                            flash('user_message', 'Vos informations ont bien été enregistrées. Vous pouvez désormais profiter du site !');

                            redirect('');
                        } else {
                            die('Something went wrong');
                        }
                    } else {
                        // Load view with errors
                        flash('user_message', 'Finalisation impossible : veuillez vérifier les erreurs trouvées.', 'alert alert-danger');
                        $this->view('/users/finalisation', $data);
                    }


                } else {
                    // Init data
                    $data = [
                        'civilite' => '',
                        'nom' => '',
                        'prenom' => '',
                        'adresse' => '',
                        'complement' => '',
                        'ville' => '',
                        'cp' => '',
                        'avatar' => '',
                        // Error messages
                        'civilite_err' => '',
                        'nom_err' => '',
                        'prenom_err' => '',
                        'adresse_err' => '',
                        'ville_err' => '',
                        'cp_err' => ''
                    ];

                    // Load view
                    $this->view('users/finalisation', $data);
                }
            } else {
                redirect('');
            }
        }

        public function compte() {
            if (isset($_SESSION['user_id']) && $_SESSION['user_civilite']) {
                $user = $this->userModel->findUserById($_SESSION['user_id']);

                if($_SERVER['REQUEST_METHOD'] == 'POST') {
                    // Sanitize POST data
                    $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);

                    // Init Data for Personal Informations Form
                    if(isset($_POST['pseudo'])) {
                        $data = [
                            'user' => $_SESSION['user_id'],
                            'pseudo' => check_input($_POST['pseudo']),
                            'avatar' => !empty($_FILES['avatar']) ? check_input($_FILES['avatar']['name']) : null,
                            'civilite' => isset($_POST['civilite']) ? $_POST['civilite'] : '',
                            'nom' => check_input($_POST['nom']),
                            'prenom' => check_input($_POST['prenom']),
                            'adresse' => check_input($_POST['adresse']),
                            'complement' => !empty($_POST['complement']) ? check_input($_POST['complement']) : null,
                            'ville' => check_input($_POST['ville']),
                            'cp' => check_input(str_replace(' ', '', $_POST['cp'])),

                            'email' => $user->email,
    
                            'email_alert' => isset($_POST['email_alert']) ? '1' : '0',
                            'newsletter_alert' => isset($_POST['newsletter_alert']) ? '1' : '0',
                            // Errors
                            'pseudo_err' => '',
                            'civilite_err' => '',
                            'nom_err' => '',
                            'prenom_err' => '',
                            'adresse_err' => '',
                            'ville_err' => '',
                            'cp_err' => '',
                            'avatar_err' => '',
                        ];

                        // Validate Pseudo
                        if(empty($data['pseudo'])) {
                            $data['pseudo_err'] = 'Veuillez entrer votre pseudo.';
                        }

                        // Validate Civilite
                        if(empty($data['civilite'])) {
                            $data['civilite_err'] = 'Veuillez sélectionner votre civilité.';
                        }

                        // Validate Nom
                        if(empty($data['nom'])) {
                            $data['nom_err'] = 'Veuillez entrer votre nom.';
                        }

                        // Validate Prenom
                        if(empty($data['prenom'])) {
                            $data['prenom_err'] = 'Veuillez entrer votre prénom.';
                        }

                        // Validate Adresse
                        if(empty($data['adresse'])) {
                            $data['adresse_err'] = 'Veuillez entrer votre adresse.';
                        }

                        // Validate Ville
                        if(empty($data['ville'])) {
                            $data['ville_err'] = 'Veuillez entrer votre ville.';
                        }

                        // Validate Code Postal
                        if(empty($data['cp'])) {
                            $data['cp_err'] = 'Veuillez entrer votre code postal.';
                        } elseif(!preg_match('/^[0-9]{5,5}$/' , $data['cp'] )) {
                            $data['cp_err'] = 'Le code postal doit contenir uniquement 5 chiffres (ex: 75000).';
                        }

                        // Check for Avatar
                        if(empty($data['avatar']) && !empty($data['civilite'])) {
                            if ($data['civilite'] =='1' && $user->avatar == 'avatar_default_man.png') {
                                // Woman avatar
                                $data['avatar'] = 'avatar_default_woman.png';
                            } elseif($data['civilite'] == '2' && $user->avatar == 'avatar_default_woman.png') {
                                // Man avatar
                                $data['avatar'] = 'avatar_default_man.png';
                            } else {
                                $data['avatar'] = $user->avatar;
                            }
                        } elseif(!empty($data['avatar'])) {
                            $res = upload_image_helper($_FILES['avatar'], '/avatars');
                            $res[0] ? $data['avatar'] = $res[1] : $data['avatar_err'] = $res[1];
                        }

                        // Make sure errors are empty
                        if(empty($data['debug']) && empty($data['pseudo_err']) && empty($data['civilite_err']) && empty($data['nom_err']) && empty($data['prenom_err']) && empty($data['adresse_err']) && empty($data['ville_err']) && empty($data['cp_err']) && empty($data['avatar_err'])) {
                            $data['new_email'] = $user->email;
                            $data['new_password'] = $user->password;
                            
                            // Update User profile
                            if($this->userModel->updateUser($data)) {
                                flash('user_message', 'Vos informations ont été mis à jour !');

                                redirect('users/compte');
                            } else {
                                die('Something went wrong');
                            }
                        } else {
                            // Load view with errors
                            flash('user_message', 'Mis à jour impossible : veuillez vérifier les erreurs trouvées.', 'alert alert-danger');
                            $this->view('users/compte', $data);
                        }
                    }

                    // Init Data for E-mail Form
                    if(isset($_POST['new_email'])) {
                        $data = [
                            'user' => $_SESSION['user_id'],
                            'pseudo' => $user->pseudo,
                            'avatar' => $user->avatar,
                            'civilite' => $user->idCivilite,
                            'nom' => $user->nom,
                            'prenom' => $user->prenom,
                            'adresse' => $user->adresse,
                            'complement' => $user->complement,
                            'ville' => $user->ville,
                            'cp' => $user->codePostal,

                            'email' => $user->email,
                            'new_email' => check_input($_POST['new_email']),
                            'password_email' => check_input($_POST['password_email']),
    
                            'email_alert' => $user->emailAlert,
                            'newsletter_alert' => $user->newsletterAlert,
                            // Errors
                            'new_email_err' => '',
                            'password_email_err' => '',
                        ];

                        // Validate new e-mail
                        if(empty($data['new_email'])) {
                            $data['new_email_err'] = 'Veuillez entrer votre nouvel email.';
                        } elseif ($this->userModel->findUserByEmail($data['new_email'])) {
                            $data['new_email_err'] = 'Cet e-mail est déjà utilisé, veuillez en choisir un autre.';
                        }

                        // Validate password
                        if(empty($data['password_email'])) {
                            $data['password_email_err'] = 'Veuillez entrer votre mot de passe.';
                        } elseif (!password_verify($data['password_email'], $user->password)) {
                            $data['password_email_err'] = 'Le mot de passe entré ne correspond pas à celui de votre compte.';
                        }

                        // Make sure errors are empty
                        if(empty($data['new_email_err']) && empty($data['password_email_err'])) {
                            $data['new_password'] = $user->password;
                            
                            // Update User profile
                            if($this->userModel->updateUser($data)) {
                                flash('user_message', 'Votre email a été mis à jour !');

                                redirect('users/compte');
                            } else {
                                die('Something went wrong');
                            }
                        } else {
                            // Load view with errors
                            flash('user_message', 'Mis à jour impossible : veuillez vérifier les erreurs trouvées.', 'alert alert-danger');
                            $this->view('users/compte', $data);
                        }
                    }

                    // Init Data for Password Form
                    if(isset($_POST['new_password'])) {
                        $data = [
                            'user' => $_SESSION['user_id'],
                            'pseudo' => $user->pseudo,
                            'avatar' => $user->avatar,
                            'civilite' => $user->idCivilite,
                            'nom' => $user->nom,
                            'prenom' => $user->prenom,
                            'adresse' => $user->adresse,
                            'complement' => $user->complement,
                            'ville' => $user->ville,
                            'cp' => $user->codePostal,

                            'email' => $user->email,
                            'new_email' => '',

                            'new_password' => check_input($_POST['new_password']),
                            'password_confirm' => check_input($_POST['password_confirm']),
                            'password' => check_input($_POST['password']),
    
                            'email_alert' => $user->emailAlert,
                            'newsletter_alert' => $user->newsletterAlert,
                            // Errors
                            'new_password_err' => '',
                            'password_confirm_err' => '',
                            'password_err' => '',
                        ];

                        // Validate Password
                        if(empty($data['new_password'])) {
                            $data['new_password_err'] = 'Veuillez entrer votre nouveau mot de passe.';
                        } elseif (strlen($data['new_password']) < 10) {
                            $data['new_password_err'] = 'Le mot de passe doit contenir 10 caractères minimum.';
                        }

                        // Validate Confirm Password
                        if(empty($data['password_confirm'])) {
                            $data['password_confirm_err'] = 'Veuillez entrer la confirmation de votre nouveau mot de passe.';
                        } elseif ($data['new_password'] != $data['password_confirm']) {
                            $data['password_confirm_err'] = 'Le mot de passe ne correspond pas au précédemment.';
                        }

                        // Validate password
                        if(empty($data['password'])) {
                            $data['password_err'] = 'Veuillez entrer votre mot de passe.';
                        } elseif (!password_verify($data['password'], $user->password)) {
                            $data['password_err'] = 'Le mot de passe entré ne correspond pas à celui de votre compte.';
                        }

                        // Make sure errors are empty
                        if(empty($data['new_password_err']) && empty($data['password_confirm_err']) && empty($data['password_err'])) {
                            $data['new_email'] = $user->email;
                            $data['new_password'] = password_hash($data['new_password'], PASSWORD_DEFAULT);
                            
                            // Update User profile
                            if($this->userModel->updateUser($data)) {
                                flash('user_message', 'Votre mot de passe a été mis à jour !');

                                redirect('users/compte');
                            } else {
                                die('Something went wrong');
                            }
                        } else {
                            // Load view with errors
                            flash('user_message', 'Mis à jour impossible : veuillez vérifier les erreurs trouvées.', 'alert alert-danger');
                            $this->view('users/compte', $data);
                        }
                    }
                } else {
                    // Init Data
                    $data = [
                        'user' => $_SESSION['user_id'],
                        'pseudo' => $user->pseudo,
                        'avatar' => $user->avatar,
                        'civilite' => $user->idCivilite,
                        'nom' => $user->nom,
                        'prenom' => $user->prenom,
                        'adresse' => $user->adresse,
                        'complement' => $user->complement,
                        'ville' => $user->ville,
                        'cp' => $user->codePostal,
    
                        'email' => $user->email,
                        'new_email' => '',
                        'password_email' => '',
    
                        'new_password' => '',
                        'password_confirm' => '',
                        'password' => '',
    
                        'email_alert' => $user->emailAlert,
                        'newsletter_alert' => $user->newsletterAlert,
    
                        // Errors
                        'pseudo_err' => '',
                        'civilite_err' => '',
                        'nom_err' => '',
                        'prenom_err' => '',
                        'adresse_err' => '',
                        'ville_err' => '',
                        'cp_err' => '',
                        'avatar_err' => '',
    
                        'new_email_err' => '',
                        'password_email_err' => '',
    
                        'new_password_err' => '',
                        'password_confirm_err' => '',
                        'password_err' => '',
                    ];
    
                    $this->view('users/compte', $data);
                }


            } else {
                redirect('users/finalisation');
            }
        }

        public function delete($id = null) {
            if ($id) {
                // Get existing mug from model
                $user = $this->userModel->findUserById($id);
    
                // Check if the mug exists
                if($user) {
                    if($_SERVER['REQUEST_METHOD'] == 'POST') {
                        // Check for owner
                        if($user->id != $_SESSION['user_id']) {
                            redirect('');
                        }
        
                        if($this->userModel->deleteUser($id)) {
                            $this->deconnexion();
                            //flash('user_message', 'Votre compte a bien été supprimée !');
                            redirect('users/connexion');
                        } else {
                            die('Something went wrong');
                        }
                    } else {
                        redirect('');
                    }
                } else {
                    redirect('');
                }
            } else {
                redirect('');
            }

        }

        // View User profile
        public function index($id = null) {
            if ($id) {
                $user =  $this->userModel->findUserById($id);

                if($_SERVER['REQUEST_METHOD'] == 'POST') {
                    $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
                    
                    $mugs = $this->mugModel->getMugsByUserId($id, $_POST['tri']);
                    
                    $data = [
                        'mugs' => $mugs,
                        'user' => $user,
                        'selected_tri' => $_POST['tri'],
                    ];
    
                    $this->view('users/index', $data);
                } else {
                    $mugs =  $this->mugModel->getMugsByUserId($id);
    
                    $data = [
                        'mugs' => $mugs,
                        'user' => $user,
                        'selected_tri' => 'plus_recents'
                    ];
                    
                    $this->view('users/index', $data);
                }
            } else {
                // No ID enter
                redirect('');
            }
        }

        // Dashboard User
        public function annonces() {
            if (isset($_SESSION['user_id']) && $_SESSION['user_civilite']) {
                $user = $this->userModel->findUserById($_SESSION['user_id']);

                $mugsPublics = [];
                $mugsAttente = [];
                $mugsRefuses = [];
                $mugsVendus = [];

                if($_SERVER['REQUEST_METHOD'] == 'POST') {
                    // Sanitize POST data
                    $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
                    $this->view('users/annonces', $data);
                } else {
                    $mugs = $this->mugModel->getMugsByUserId($_SESSION['user_id']);

                    if($mugs) {
                        foreach ($mugs as $mug) {
                            $mug->createdAt = date('d/m/Y', strtotime($mug->createdAt));

                            if($mug->updatedAt) {
                                $mug->updatedAt = date('d/m/Y', strtotime($mug->updatedAt));
                            }

                            switch ($mug->idStatut) {
                                case '1':
                                    array_push($mugsPublics, $mug);
                                    break;
                                case '2':
                                    array_push($mugsAttente, $mug);
                                    break;
                                case '3':
                                    array_push($mugsRefuses, $mug);
                                    break;
                                case '4':
                                    array_push($mugsVendus, $mug);
                                    break;
                            }
                        }
                    }

                    $data = [
                        'mugs_publics' => $mugsPublics,
                        'mugs_attente' => $mugsAttente,
                        'mugs_refuses' => $mugsRefuses,
                        'mugs_vendus' => $mugsVendus
                    ];
    
                    $this->view('users/annonces', $data);
                }
            } else {
                redirect('users/finalisation');
            }
        }

        public function panier() {
            // Check if user is logged
            if(!isset($_SESSION['user_id'])) {
                redirect('users/connexion');
            }

            // Get Mugs on the User Panier
            $mugs = $this->mugModel->getMugsInPanier($_SESSION['user_id']);

            // Get total price
            $prixTotal = 0;
            foreach ($mugs as $mug) {
                $prixTotal += $mug->prixHt;
            }
            $prixTotal = number_format($prixTotal + $prixTotal * 0.20, 2, ',', ' ');

            if($_SERVER['REQUEST_METHOD'] == 'POST') {
                
            } else {
                $data = [
                    'mugs' => $mugs,
                    'prix_total' => $prixTotal
                ];
    
                $this->view('users/panier', $data);
            }
        }

        public function deleteRowPanier($id = null) {
            if(!$id) {
                redirect('');
            }

            // Check if user is logged
            if(!isset($_SESSION['user_id'])) {
                redirect('users/connexion');
            }

            $idOwner = $this->mugModel->getRowPanier($id);
            // Check for owner
            if($idOwner->idUser != $_SESSION['user_id']) {
                redirect('');
            }

            if($_SERVER['REQUEST_METHOD'] == 'POST') {
                
                if ($this->mugModel->deleteRowPanier($id)) {
                    flash('user_message', 'Le mug vient a bien été retiré du panier !');
                    redirect('users/panier');
                } else {
                    die('Something went wrong');
                }
            }

            redirect('users/panier');
        }

        public function paiement() {
            // Check if user is logged
            if(!isset($_SESSION['user_id'])) {
                redirect('users/connexion');
            }

            $user = $this->userModel->findUserById($_SESSION['user_id']);
            $panier = $this->mugModel->getMugsInPanier($_SESSION['user_id']);
            $livraison = $this->userModel->getTypesLivraison();

            // Get total price
            $prixTotal = 0;
            foreach ($panier as $mug) {
                $prixTotal += $mug->prixHt;
            }
            $prixTotal = $prixTotal + $prixTotal * 0.20;

            if($_SERVER['REQUEST_METHOD'] == 'POST') {
                // Get all mugs id of the command
                $idMugs = [];
                foreach ($panier as $mug) {
                    array_push($idMugs, $mug->id);
                }

                $data = [
                    'id_client' => $user->id,
                    'id_livraison' => $_POST['livraisonRadios'],
                    'id_mugs' => $idMugs,
                    'panier' => $panier,
                ];

                if($this->userModel->addCommand($data)) {
                    flash('user_message', 'Nous vous remercions pour votre achat !');
                    redirect('');
                } else {
                    die('Something went wrong');
                }
            } else {
                // redirect('users/panier');
                $data = [
                    'user' => $user,
                    'panier' => $panier,
                    'prix_total' => $prixTotal,
                    'livraison' => $livraison
                ];
                $this->view('users/paiement', $data);
            }

        }
    }