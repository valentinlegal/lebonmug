<?php
    // Load Config
    require_once 'config/config.php';

    // Load Helpers
    require_once 'helpers/url_helper.php';
    require_once 'helpers/check_input_helper.php';
    require_once 'helpers/session_helper.php';
    require_once 'helpers/console_log_helper.php';
    require_once 'helpers/upload_image_helper.php';

    // Autoload Core Libraries
    spl_autoload_register(function($className) {
        require_once 'libraries/' . $className . '.php';
    });