    </div>

    <footer class="p-3 bg-dark text-white">
        <div class="row">
            <div class="col-12 mb-1">
                <a href="<?=URLROOT?>">LeBonMug</a> 2020 — Développé par <a href="https://valentinlegal.fr" target="_blank">Valentin Le Gal</a> avec ☕️ & ❤️
            </div>
            <div class="col">
                Retrouvez le code source de ce site sur <a href="https://gitlab.com/ValentinLeGal/lebonmug" target="_blank">GitLab</a> !
            </div>
            <div class="col-auto">
                Version <?=APPVERSION?>
            </div>
        </div>
    </footer>
    
    <script src="<?=URLROOT?>/js/jquery-3.4.1.js"></script>
    <script src="<?=URLROOT?>/js/bootstrap.min.js"></script>
    <script src="<?=URLROOT?>/js/main.js"></script>
</body>
</html>