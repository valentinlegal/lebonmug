<?php require APPROOT . '/views/inc/header.php';?>

<div class="row">
    <div class="col-md-6 mb-md-4 mx-auto">
        <!-- Alert -->
        <div class="row">
            <div class="col-md-12 mx-auto">
                <?=flash('user_message')?>
            </div>
        </div>

        <h1 class="text-center mb-md-4">Mon compte</h1>

        <!-- Info Form -->
        <form action="<?=URLROOT?>/users/compte" method="post" enctype="multipart/form-data">
            <h2 class="mb-3">Informations personnelles</h2>

            <!-- Pseudo -->
            <div class="form-group">
                <div class="row">
                    <label class="col-auto mr-auto" for="pseudo">Nom d'affichage</label>
                    <small class="col-auto">Champ requis</small>
                </div>
                <input class="form-control <?=(!empty($data['pseudo_err'])) ? 'is-invalid' : ''?>" type="text" name="pseudo" aria-describedby="displayNameHelp" placeholder="ex : Pierre" value="<?=$data['pseudo']?>">
                <div class="invalid-feedback"><?=$data['pseudo_err']?></div>
                <small class="form-text text-muted" id="displayNameHelp">Ce nom sera visible par tous.</small>
            </div>
            
            <div class="form-group">
                <div class="form-row">

                    <!-- Avatar -->
                    <div class="col mr-3">
                        <div class="row">
                            <label class="col-auto mr-auto" for="avatar">Avatar</label>
                        </div>
                        <div class="custom-file">
                            <input type="file" class="custom-file-input <?=(!empty($data['avatar_err'])) ? 'is-invalid' : ''?>" name="avatar" aria-describedby="avatarHelp">
                            <label class="custom-file-label" for="avatarFile" data-browse="Parcourir...">Choisissez un avatar</label>
                            <div class="invalid-feedback"><?=$data['avatar_err']?></div>
                        </div>
                        <small class="form-text text-muted" id="avatarHelp">Fichiers acceptés : .png, .jpg, .jpeg</small>
                    </div>

                    <!-- Civilite -->
                    <div class="col">
                        <div class="row">
                            <label class="col-auto mr-auto" for="civilite">Civilité</label>
                            <small class="col-auto">Champ requis</small>
                        </div>
                        <select class="custom-select <?=(!empty($data['civilite_err'])) ? 'is-invalid' : ''?>" name="civilite">
                            <option <?=($data['civilite'] == '') ? 'selected' : ''?> disabled>Choisissez votre civilité...</option>
                            <option value="1" <?=($data['civilite'] == 1) ? 'selected' : ''?>>Madame</option>
                            <option value="2" <?=($data['civilite'] == 2) ? 'selected' : ''?>>Monsieur</option>
                        </select>
                        <div class="invalid-feedback"><?=$data['civilite_err']?></div>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <div class="form-row">

                    <!-- Nom -->
                    <div class="col mr-3">
                        <div class="row">
                            <label class="col-auto mr-auto" for="nom">Nom</label>
                            <small class="col-auto">Champ requis</small>
                        </div>
                        <input class="form-control <?=(!empty($data['nom_err'])) ? 'is-invalid' : ''?>" type="text" name="nom" placeholder="ex : Dupont" value="<?=$data['nom']?>">
                        <div class="invalid-feedback"><?=$data['nom_err']?></div>
                    </div>

                    <!-- Prénom -->
                    <div class="col">
                        <div class="row">
                            <label class="col-auto mr-auto" for="prenom">Prénom</label>
                            <small class="col-auto">Champ requis</small>
                        </div>
                        <input class="form-control <?=(!empty($data['prenom_err'])) ? 'is-invalid' : ''?>" type="text" name="prenom" placeholder="ex : Pierre" value="<?=$data['prenom']?>">
                        <div class="invalid-feedback"><?=$data['prenom_err']?></div>
                    </div>
                </div>
            </div>

            <!-- Adresse -->
            <div class="form-group">
                <div class="row">
                    <label class="col-auto mr-auto" for="adresse">Adresse</label>
                    <small class="col-auto">Champ requis</small>
                </div>
                <input class="form-control <?=(!empty($data['adresse_err'])) ? 'is-invalid' : ''?>" type="text" name="adresse" placeholder="ex : 17 rue des Lys" value="<?=$data['adresse']?>">
                <div class="invalid-feedback"><?=$data['adresse_err']?></div>
            </div>

            <!-- Complément -->
            <div class="form-group">
                <div class="row">
                    <label class="col-auto mr-auto" for="complement">Complément</label>
                </div>
                <input class="form-control" type="text" name="complement" aria-describedby="complementHelp" placeholder="ex : Appart. 215, Étage 3, etc." value="<?=$data['complement']?>">
                <small class="form-text text-muted" id="complementHelp">Toutes informations utiles à la livraison.</small>
            </div>

            <div class="form-group">
                <div class="form-row">
                    <!-- Ville -->
                    <div class="col mr-3">
                        <div class="row">
                            <label class="col-auto mr-auto" for="ville">Ville</label>
                            <small class="col-auto">Champ requis</small>
                        </div>
                        <input class="form-control <?=(!empty($data['ville_err'])) ? 'is-invalid' : ''?>" type="text" name="ville" placeholder="ex : La Rochelle" value="<?=$data['ville']?>">
                        <div class="invalid-feedback"><?=$data['ville_err']?></div>
                    </div>

                    <!-- Code postal -->
                    <div class="col">
                        <div class="row">
                            <label class="col-auto mr-auto" for="cp">Code Postal</label>
                            <small class="col-auto">Champ requis</small>
                        </div>
                        <input class="form-control <?=(!empty($data['cp_err'])) ? 'is-invalid' : ''?>" type="text" name="cp" placeholder="ex : 17000" value="<?=$data['cp']?>">
                        <div class="invalid-feedback"><?=$data['cp_err']?></div>
                    </div>
                </div>
            </div>

            <!-- Messagerie -->
            <div class="form-group form-check">
                <input class="form-check-input" type="checkbox" name="email_alert" <?=($data['email_alert'] == '1') ? 'checked' : ''?>>
                <label class="form-check-label" id="email_alert">Recevoir un mail lors de la réception d'un nouveau message sur votre messagerie</label>
            </div>

            <!-- Newsletter -->
            <div class="form-group form-check">
                <input class="form-check-input" type="checkbox" name="newsletter_alert" <?=($data['newsletter_alert'] == '1') ? 'checked' : ''?>>
                <label class="form-check-label" id="newsletter_alert">Recevoir un mail pour connaître tous nos bons plans (newsletter)</label>
            </div>
            
            <!-- Submit -->
            <div class="row no-gutters">
                <div class="col mr-3"><button class="btn btn-primary btn-block" type="submit">Enregister</button></div>
                <a class="col btn btn-secondary btn-block" href="<?=URLROOT?>">Annuler</a>
            </div>
        </form>


        <hr class="my-4">


        <!-- Email Form -->
        <form action="<?=URLROOT?>/users/compte" method="post">
            <h2 class="mb-3">Mon email</h2>

            <!-- E-mail actuel -->
            <div class="form-group">
                <div class="row">
                    <label class="col-auto mr-auto" for="email">E-mail actuel</label>
                    <small class="col-auto">Champ requis</small>
                </div>
                <input class="form-control disabled" type="email" name="email" value="<?=$data['email']?>" disabled>
            </div>

            <!-- Nouvel e-mail -->
            <div class="form-group">
                <div class="row">
                    <label class="col-auto mr-auto" for="new_email">Nouvel e-mail</label>
                    <small class="col-auto">Champ requis</small>
                </div>
                <input class="form-control <?=(!empty($data['new_email_err'])) ? 'is-invalid' : ''?>" type="email" name="new_email" placeholder="ex : pierre.dupont@compagnie.com" value="<?=$data['new_email']?>">
                <div class="invalid-feedback"><?=$data['new_email_err']?></div>
            </div>

            <!-- Mot de passe -->
            <div class="form-group">
                <div class="row">
                    <label class="col-auto mr-auto" for="password_email">Mot de passe actuel</label>
                    <small class="col-auto">Champ requis</small>
                </div>
                <input class="form-control <?=(!empty($data['password_email_err'])) ? 'is-invalid' : ''?>" type="password" name="password_email">
                <div class="invalid-feedback"><?=$data['password_email_err']?></div>
            </div>
            
            <!-- Submit -->
            <div class="row no-gutters">
                <div class="col mr-3"><button class="btn btn-primary btn-block" type="submit">Modifier l'e-mail</button></div>
                <a class="col btn btn-secondary btn-block" href="<?=URLROOT?>">Annuler</a>
            </div>
        </form>


        <hr class="my-4">


        <!-- Password Form -->
        <form action="<?=URLROOT?>/users/compte" method="post">
            <h2 class="mb-3">Mon mot de passe</h2>

            <!-- Nouveau mot de passe -->
            <div class="form-group">
                <div class="row">
                    <label class="col-auto mr-auto" for="new_password">Nouveau mot de passe</label>
                    <small class="col-auto">Champ requis</small>
                </div>
                <input class="form-control <?=(!empty($data['new_password_err'])) ? 'is-invalid' : ''?>" type="password" name="new_password" aria-describedby="passwordHelp1">
                <div class="invalid-feedback"><?=$data['new_password_err']?></div>
                <small class="form-text text-muted" id="passwordHelp1">Minimum 10 caractères.</small>
            </div>

            <!-- Confirmer mot de passe -->
            <div class="form-group">
                <div class="row">
                    <label class="col-auto mr-auto" for="password_confirm">Confirmer le mot de passe</label>
                    <small class="col-auto">Champ requis</small>
                </div>
                <input class="form-control <?=(!empty($data['password_confirm_err'])) ? 'is-invalid' : ''?>" type="password" name="password_confirm" aria-describedby="passwordHelp2">
                <div class="invalid-feedback"><?=$data['password_confirm_err']?></div>
                <small class="form-text text-muted" id="passwordHelp2">Doit correspondre au mot de passe entré précédemment.</small>
            </div>

            <!-- Mot de passe actuel -->
            <div class="form-group">
                <div class="row">
                    <label class="col-auto mr-auto" for="password">Mot de passe actuel</label>
                    <small class="col-auto">Champ requis</small>
                </div>
                <input class="form-control <?=(!empty($data['password_err'])) ? 'is-invalid' : ''?>" type="password" name="password">
                <div class="invalid-feedback"><?=$data['password_err']?></div>
            </div>
            
            <!-- Submit -->
            <div class="row no-gutters">
                <div class="col mr-3"><button class="btn btn-primary btn-block" type="submit">Modifier le mot de passe</button></div>
                <a class="col btn btn-secondary btn-block" href="<?=URLROOT?>">Annuler</a>
            </div>
        </form>


        <hr class="my-4">


        <form action="<?=URLROOT?>/users/delete/<?=$data['user']?>" method="post">
            <div class="row">
                <div class="col-6 ml-auto">
                    <button type="button" class="btn btn-outline-danger btn-block" data-toggle="modal" data-target="#deleteModal">
                        Supprimer mon compte
                    </button>
                </div>
            </div>

            <!-- Modal -->
            <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="deleteModalLabel">Avertissement</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <p>En supprimant votre compte, toutes les annonces qui vous sont associées seront aussi supprimées du site.</p>
                        <p><strong>Cette action est irréversible.</strong></p>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-secondary mr-3" type="button" data-dismiss="modal">Annuler</button>
                        <button class="btn btn-danger" type="submit" value="Delete">Supprimer mon compte</button>
                    </div>
                    </div>
                </div>
            </div>
        </form>

    </div>
</div>

<?php require APPROOT . '/views/inc/footer.php';?>