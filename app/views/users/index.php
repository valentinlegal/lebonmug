<?php require APPROOT . '/views/inc/header.php';?>

<div class="row mb-4">
    <div class="col-auto ml-auto mr-2 rounded-circle bg-img " style="width: 3.5em; height: 3.5em; background-image: url('<?=URLROOT?>/img/avatars/<?=$data['user']->avatar?>"></div>
    <h1 class="col-auto mr-auto text-center mb-4"><?=$data['user']->pseudo?></h1>
</div>

<div class="row-cols-1">
    <div class="col-md-8 mx-auto">
        <div class="row no-gutters">
            <h2 class="col">Mugs en vente <span class="text-muted">(<?=count($data['mugs'])?>)</span></h2>
            <form class="col-auto row" action="<?=URLROOT?>/users/<?=$data['user']->id?>" method="post">
                <select class="col custom-select" name="tri">
                    <option value="plus_recents" <?=$data['selected_tri'] == 'plus_recents' ? 'selected' : ''?>>Tri : Plus récents</option>
                    <option value="plus_anciens" <?=$data['selected_tri'] == 'plus_anciens' ? 'selected' : ''?>>Tri : Plus anciens</option>
                    <option value="prix_croissants" <?=$data['selected_tri'] == 'prix_croissants' ? 'selected' : ''?>>Tri : Prix croissants</option>
                    <option value="prix_decroissants" <?=$data['selected_tri'] == 'prix_decroissants' ? 'selected' : ''?>>Tri : Prix décroissants</option>
                </select>
                <div class="col-auto"><input class="col btn btn-secondary" type="submit" value="Trier"></div>
            </form>
        </div>
        
        <div>
            <?php if($data['mugs']):?>
            <?php foreach($data['mugs'] as $mug): ?>
                <?php if($mug->idStatut == 1): ?>
                    <div class="card my-4">
                        <div class="row no-gutters">
                            <div class="col-4 bg-img" style="background-image: url('<?=URLROOT?>/img/mugs/<?=$mug->photo1?>');"></div>
                            <div class="col-8">
                                <div class="card-body">
                                    <h5 class="card-title mb-2"><?=$mug->titre?></h5>
                                    <span class="badge badge-primary mb-2"><?=$mug->prixTtc?>€</span>
                                    <p class="card-text text-truncate"><?=$mug->description?></p>
                                    <p class="card-text"><small class="text-muted"><?=date('d/m/Y \à H:i', strtotime($mug->createdAt))?></small></p>
                                    <a class="btn btn-primary" href="<?=URLROOT?>/mugs/<?=$mug->id?>">En savoir plus</a>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endif; ?>
            <?php endforeach; ?>
            
            <?php else: ?>
                <p class="mt-4 text-center font-italic text-muted">Aucun mug trouvé pour cet utilisateur.</p>
            <?php endif; ?>
        </div>
        
    </div>
</div>


<?php require APPROOT . '/views/inc/footer.php';?>