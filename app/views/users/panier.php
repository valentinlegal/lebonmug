<?php require APPROOT . '/views/inc/header.php';?>

<div class="row-cols-1">
    <div class="col-md-10 mx-auto">

    <div class="row">
        <div class="col-12">
            <?=flash('user_message')?>
            <?=flash('mug_message')?>
        </div>
    </div>

    <h1 class="text-center mb-5">Panier</h1>
        <?php if($data['mugs']): ?>
        <table class="table table-striped mt-4">
            <thead>
                <tr>
                    <th scope="col"></th>
                    <th scope="col">Titre</th>
                    <th scope="col">Vendu par</th>
                    <th scope="col">Ajouté le</th>
                    <th scope="col">Prix (TTC)</th>
                    <th scope="col"></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach($data['mugs'] as $mug): ?>
                    <tr>
                        <th class="align-middle" scope="row">
                            <div class="bg-dark rounded bg-img" style="width: 2em; height: 2em; background-image: url('<?=URLROOT?>/img/mugs/<?=$mug->photo1?>');"></div>
                        </th>
                        <td class="align-middle">
                            <a href="<?=URLROOT?>/mugs/<?=$mug->id?>"><?=$mug->titre?></a>
                        </td>
                        <td class="align-middle">
                            <a href="<?=URLROOT?>/users/<?=$mug->idVendeur?>"><?=$mug->vendeur?></a>
                        </td>
                        <td class="align-middle">
                            <?=date('d/m/Y', strtotime($mug->createdAt))?>
                        </td>
                        <td class="align-middle">
                            <?=number_format($mug->prixHt + $mug->prixHt * 0.20, 2, ',', ' ')?>€
                        </td>
                        <td class="align-middle text-right">
                            <form class="text-right" action="<?=URLROOT?>/users/deleteRowPanier/<?=$mug->idLignePanier?>" method="post">
                                <button class="btn btn-danger" type="button" data-toggle="modal" data-target="#deleteModal">Retirer</button>
                                <!-- Modal -->
                                <div class="modal fade text-left" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="deleteModalLabel">Avertissement</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <p>Êtes-vous sûr de vouloir retirer ce mug du panier ?</p>
                                            <p><strong>Cette action est irréversible.</strong></p>
                                        </div>
                                        <div class="modal-footer">
                                            <button class="btn btn-secondary mr-3" type="button" data-dismiss="modal">Annuler</button>
                                            <button class="btn btn-danger" type="submit" value="Delete">Retirer ce mug</button>
                                        </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
        
        <hr class="my-5">

        <!-- R E S U M E -->
        <div class="row no-gutters">
            <h4 class="col mr-auto"><?=count($data['mugs'])?> mug<?=count($data['mugs']) > 1 ? 's' : ''?> dans ce panier.</h4>
            <h4 class="col mr-auto text-right">Prix total (TTC) : <?=$data['prix_total']?>€</h4>
        </div>

        <form action="<?=URLROOT?>/users/paiement" class="mt-5">
            <button class="btn btn-primary btn-block" type="submit">Confirmer et passer au paiement</button>
        </form>

        <?php else: ?>
            <p class="mt-4 text-center font-italic text-muted">Aucun mug dans le panier.</p>
        <?php endif; ?>

    </div>
</div>

<?php require APPROOT . '/views/inc/footer.php';?>