<?php require APPROOT . '/views/inc/header.php';?>

<!-- Alert -->
<div class="row">
    <div class="col-md-4 mx-auto">
        <?=flash('user_message')?>
    </div>
</div>

<h1 class="text-center">Connexion</h1>

<div class="row">
    <div class="col-md-4 my-md-4 mx-auto">
        <form action="<?=URLROOT?>/users/connexion" method="post">

            <!-- E-mail -->
            <div class="form-group">
                <div class="row">
                    <label class="col-auto mr-auto" for="emailInput">E-mail</label>
                    <small class="col-auto">Champ requis</small>
                </div>
                <input class="form-control <?=(!empty($data['email_err'])) ? 'is-invalid' : ''?>" type="email" name="emailInput" placeholder="ex : pierre@compagnie.com" value="<?=$data['email']?>">
                <div class="invalid-feedback"><?=$data['email_err']?></div>
            </div>

            <!-- Password -->
            <div class="form-group">
                <div class="row">
                    <label class="col-auto mr-auto" for="passwordInput">Mot de passe</label>
                    <small class="col-auto">Champ requis</small>
                </div>
                <input class="form-control <?=(!empty($data['pass_err'])) ? 'is-invalid' : ''?>" type="password" name="passwordInput" value="<?=$data['pass']?>">
                <div class="invalid-feedback"><?=$data['pass_err']?></div>
            </div>

            <!-- Submit form -->
            <input class="btn btn-primary btn-block" type="submit" value="Se connecter">
        </form>

        <hr>

        <p class="text-center">Vous n'avez pas de compte ?</p>
        <a class="btn btn-outline-primary btn-block" href="<?=URLROOT?>/users/inscription">Créer un compte</a>
    </div>
</div>

<?php require APPROOT . '/views/inc/footer.php';?>