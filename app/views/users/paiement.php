<?php require APPROOT . '/views/inc/header.php';?>

<div class="row-cols-1">
    <div class="col-md-10 mx-auto">

    <div class="row">
        <div class="col-12">
            <?=flash('user_message')?>
            <?=flash('mug_message')?>
        </div>
    </div>

    <h1 class="text-center mb-4">Paiement</h1>

        <form action="<?=URLROOT?>/users/paiement" method="post" name="paiement">
            <div class="row no-gutters">
                <div class="col card mr-4">
                    <div class="card-header">Livraison</div>
                    <div class="card-body">
                        <h5 class="card-title"><?=$data['user']->idCivilite == '1' ? 'Madame' : 'Monsieur'?> <?=$data['user']->nom?> <?=$data['user']->prenom?></h5>
                        <div class="card-text">
                            <ul class="pl-0" style="list-style: none;">
                                <li class="mb-1"><?=$data['user']->adresse?></li>
                                <?php if($data['user']->complement): ?>
                                <li class="mb-1"><?=$data['user']->complement?></li>
                                <?php endif; ?>
                                <li class="mb-1"><?=$data['user']->ville?>, <?=$data['user']->codePostal?>, France</li>
                            </ul>
                        </div>
                        
                        <hr>
                        
                        <?php foreach($data['livraison'] as $livraison): ?>
                        <div class="card-text form-check">
                            <input class="form-check-input" type="radio" name="livraisonRadios" onclick="handleClick(this)" id="standardRadio" value="<?=$livraison->id?>" <?=$livraison->id == 1 ? 'checked' : ''?>>
                            <label class="form-check-label" for="standardRadio">
                                <?=$livraison->libelle?> <span class="badge <?=$livraison->coutTtc == 0 ? 'badge-success' : 'badge-warning'?>"><?=$livraison->coutTtc == 0 ? 'Gratuit' : '+ ' . $livraison->coutTtc . '€ TTC'?></span>
                                <small class="d-block text-muted mt-1">Livraison estimée au <b><?=date('d/m/Y', strtotime(date('Y-m-d') . ' + ' . $livraison->joursLivraison . ' days'))?></b>.</small>
                            </label>
                        </div>
                        <?php endforeach;?>
                    </div>
                </div>
    
                <div class="col card">
                    <div class="card-header">Commande</div>
                    <div class="card-body">
                        <h5 class="card-title">Articles</h5>
                        <div class="card-text">
                            <ul class="pl-4" style="list-style: none;">
                                <?php foreach($data['panier'] as $mug): ?>
                                <li class="row no-gutters mb-1">
                                    <div class="col mr-3"><?=$mug->titre?></div>
                                    <div class="col-auto"><?=number_format($mug->prixHt + $mug->prixHt * 0.20, 2, ',', ' ')?>€</div>
                                </li>
                                <?php endforeach;?>
                            </ul>
                        </div>
    
                        <h5 class="card-title">Livraison</h5>
                        <div class="card-text">
                            <ul class="pl-4" style="list-style: none;">
                                <li class="row no-gutters mb-1">
                                    <div class="col mr-3" id="typeLivraison">Livraison standard</div>
                                    <div class="col-auto" id="coutLivraison">0,00 €</div>
                                </li>
                            </ul>
                        </div>
    
                        <div class="row no-gutters">
                            <h5 class="col card-title">Total (TTC)</h5>
                            <h5 class="col-auto ml-auto card-title"><span id="prixTotal"><?=number_format($data['prix_total'], 2, ',', ' ')?></span>€</h5>
                        </div>
    
                        <button class="btn btn-primary btn-block mt-2" type="button" data-toggle="modal" data-target="#payModal">Payer</button>
                        <p class="text-muted mt-1" style="line-height: 1em;"><small class="font-italic">Ce site est une simulation, vous ne recevrez et ne paierez rien.</small></p>

                        <!-- Modal -->
                        <div class="modal fade text-left" id="payModal" tabindex="-1" role="dialog" aria-labelledby="payModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="payModalLabel">Confirmation</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <p>Êtes-vous sûr de vouloir passer cette commande ?</p>
                                    <p class="text-muted"><em>Pour rappel, ce site est une simulation. Vous ne recevrez et ne paierez rien.</em></p>
                                </div>
                                <div class="modal-footer">
                                    <button class="btn btn-secondary mr-3" type="button" data-dismiss="modal">Annuler</button>
                                    <button class="btn btn-primary" type="submit" value="Payer">Payer</button>
                                </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>

        <hr class="my-4">

        <!-- Annuler commande -->
        <a class="btn btn-secondary btn-block" href="<?=URLROOT?>/users/panier">Retour au panier</a>

        <script>

            function handleClick(myRadio) {
                if(myRadio.value == 1) {
                    document.getElementById('typeLivraison').innerHTML = 'Livraison Standard';
                    document.getElementById('coutLivraison').innerHTML = '0,00€';
                    document.getElementById('prixTotal').innerHTML = (<?=$data['prix_total']?> + 0).toFixed(2).replace(/\d(?=(\d{3})+\,)/g, '$& ');
                    
                } else {
                    document.getElementById('typeLivraison').innerHTML = 'Livraison Express';
                    document.getElementById('coutLivraison').innerHTML = '5,00€';
                    document.getElementById('prixTotal').innerHTML = (<?=$data['prix_total']?> + 5).toFixed(2).replace(/\d(?=(\d{3})+\,)/g, '$& ');
                }
            }
        </script>

    </div>
</div>

<?php require APPROOT . '/views/inc/footer.php';?>