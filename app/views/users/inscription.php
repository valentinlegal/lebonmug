<?php require APPROOT . '/views/inc/header.php';?>

<h1 class="text-center">Inscription</h1>

<div class="row">
    <div class="col-md-4 my-md-4 mx-auto">
        <form action="<?=URLROOT?>/users/inscription" method="post">

            <!-- Pseudo -->
            <div class="form-group">
                <div class="row">
                    <label class="col-auto mr-auto" for="pseudoInput">Nom d'affichage</label>
                    <small class="col-auto">Champ requis</small>
                </div>
                <input class="form-control <?=(!empty($data['pseudo_err'])) ? 'is-invalid' : ''?>" type="text" name="pseudoInput" aria-describedby="pseudoHelp" placeholder="ex : Pierre" value="<?=$data['pseudo']?>">
                <div class="invalid-feedback"><?=$data['pseudo_err']?></div>
                <small class="form-text text-muted" id="pseudoHelp">Ce nom sera visible par tous.</small>
            </div>

            <!-- E-mail -->
            <div class="form-group">
                <div class="row">
                    <label class="col-auto mr-auto" for="emailInput">E-mail</label>
                    <small class="col-auto">Champ requis</small>
                </div>
                <input class="form-control <?=(!empty($data['email_err'])) ? 'is-invalid' : ''?>" type="email" name="emailInput" aria-describedby="emailHelp" placeholder="ex : pierre@compagnie.com" value="<?=$data['email']?>">
                <div class="invalid-feedback"><?=$data['email_err']?></div>
                <small class="form-text text-muted" id="emailHelp">Nous ne partagerons jamais votre e-mail.</small>
            </div>

            <!-- Password -->
            <div class="form-group">
                <div class="row">
                    <label class="col-auto mr-auto" for="passwordInput1">Mot de passe</label>
                    <small class="col-auto">Champ requis</small>
                </div>
                <input class="form-control <?=(!empty($data['pass_err'])) ? 'is-invalid' : ''?>" type="password" name="passwordInput1" aria-describedby="passwordHelp1" value="<?=$data['pass']?>">
                <div class="invalid-feedback"><?=$data['pass_err']?></div>
                <small class="form-text text-muted" id="passwordHelp1">Minimum 10 caractères.</small>
            </div>

            <!-- Confirm password -->
            <div class="form-group">
                <div class="row">
                    <label class="col-auto mr-auto" for="passwordInput2">Confirmer le mot de passe</label>
                    <small class="col-auto">Champ requis</small>
                </div>
                <input class="form-control <?=(!empty($data['confirm_pass_err'])) ? 'is-invalid' : ''?>" type="password" name="passwordInput2" aria-describedby="passwordHelp2" value="<?=$data['confirm_pass']?>">
                <div class="invalid-feedback"><?=$data['confirm_pass_err']?></div>
                <small class="form-text text-muted" id="passwordHelp2">Doit correspondre au mot de passe entré précédemment.</small>
            </div>

            <!-- Newsletter -->
            <div class="form-group form-check">
                <input class="form-check-input" type="checkbox" name="newsletterCheck" <?=($data['accept_newsletter'] == 'true') ? 'checked' : ''?>>
                <label class="form-check-label" for="newsletterCheck">Recevoir tous nos bons plans par mail (newsletter)</label>
            </div>

            <!-- Conditions -->
            <div class="form-group form-check">
                <input class="form-check-input <?=(!empty($data['accept_conditions_err'])) ? 'is-invalid' : ''?>" type="checkbox" name="conditionsCheck" <?=($data['accept_conditions'] == 'true') ? 'checked' : ''?>>
                <label class="form-check-label" for="conditionsCheck">« J'accepte les <a href="<?=URLROOT?>/conditions/conditions-generales-vente" target="_blank">Conditions Générales de Vente</a> et les <a href="<?=URLROOT?>/conditions/conditions-generales-utilisation" target="_blank">Conditions Générales d'Utilisation</a> »</label>
                <div class="invalid-feedback"><?=$data['accept_conditions_err']?></div>
            </div>

            <!-- Submit form -->
            <input class="btn btn-primary btn-block" type="submit" value="Créer mon compte">
        </form>

        <hr>

        <p class="text-center">Vous avez déjà un compte ?</p>
        <a class="btn btn-outline-primary btn-block" href="<?=URLROOT?>/users/connexion">Se connecter</a>
    </div>
</div>

<?php require APPROOT . '/views/inc/footer.php';?>