<?php require APPROOT . '/views/inc/header.php';?>

<div class="row-cols-1 mb-md-4">
    <form class="col-md-8 mx-auto" action="<?=URLROOT?>/mugs/add" method="post" enctype="multipart/form-data">
        <!-- Alert -->
        <div class="row">
            <div class="col-md-12 mx-auto">
                <?=flash('user_message')?>
            </div>
        </div>

        <h1 class="text-center mb-md-4">Déposer une annonce</h1>

        <!-- Titre -->
        <div class="form-group">
            <div class="row">
                <label class="col-auto mr-auto" for="titre">Titre</label>
                <small class="col-auto">Champ requis</small>
            </div>
            <input class="form-control <?=(!empty($data['titre_err'])) ? 'is-invalid' : ''?>" type="text" name="titre" placeholder="ex : Mug Smiley" value="<?=$data['titre']?>">
            <div class="invalid-feedback"><?=$data['titre_err']?></div>
        </div>

        <!-- Description -->
        <div class="form-group">
            <div class="row">
                <label class="col-auto mr-auto" for="description">Description</label>
                <small class="col-auto">Champ requis</small>
            </div>
            <textarea class="form-control <?=(!empty($data['description_err'])) ? 'is-invalid' : ''?>" name="description" rows="3"><?=$data['description']?></textarea>
            <div class="invalid-feedback"><?=$data['description_err']?></div>
        </div>

        <!-- Prix -->
        <div class="form-group">
            <div class="row">
                <label class="col-auto mr-auto" for="prix">Prix (HT)</label>
                <small class="col-auto">Champ requis</small>
            </div>
            <div class="input-group">
                <input class="form-control <?=(!empty($data['prix_err'])) ? 'is-invalid' : ''?>" type="text" name="prix" placeholder="ex : 9,50" value="<?=$data['prix']?>">
                <div class="input-group-append">
                    <span class="input-group-text">€</span>
                </div>
                <div class="invalid-feedback"><?=$data['prix_err']?></div>
            </div>
        </div>

        <div class="form-group">
            <div class="form-row">
                
                <!-- Couleur -->
                <div class="col mr-3">
                    <label for="couleur">Couleur (principale)</label>
                    <select class="custom-select <?=(!empty($data['couleur_err'])) ? 'is-invalid' : ''?>" name="couleur">
                        <option <?=($data['etat'] == '') ? 'selected' : ''?> disabled>Choisissez la couleur...</option>
                        <?php foreach($data['select_couleurs'] as $couleur): ?>
                            <option <?=($data['couleur'] == $couleur->id) ? 'selected' : ''?> value="<?=$couleur->id?>"><?=$couleur->libelle?></option>
                        <?php endforeach; ?>
                    </select>
                    <div class="invalid-feedback"><?=$data['couleur_err']?></div>
                </div>

                <!-- Type -->
                <div class="col">
                    <label for="type">Type</label>
                    <select class="custom-select <?=(!empty($data['type_err'])) ? 'is-invalid' : ''?>" name="type">
                        <option <?=($data['type'] == '') ? 'selected' : ''?> disabled>Choisissez le type...</option>
                        <?php foreach($data['select_types'] as $type): ?>
                            <option <?=($data['type'] == $type->id) ? 'selected' : ''?> value="<?=$type->id?>"><?=$type->libelle?></option>
                        <?php endforeach; ?>
                    </select>
                    <div class="invalid-feedback"><?=$data['type_err']?></div>
                </div>
            </div>
        </div>
        
        <div class="form-group">
            <div class="form-row">

                <!-- Volume -->
                <div class="col mr-3">
                    <label for="volume">Volume (approximatif)</label>
                    <select class="custom-select <?=(!empty($data['volume_err'])) ? 'is-invalid' : ''?>" name="volume">
                        <option <?=($data['volume'] == '') ? 'selected' : ''?> disabled>Choisissez le volume...</option>
                        <?php foreach($data['select_volumes'] as $volume): ?>
                            <option <?=($data['volume'] == $volume->id) ? 'selected' : ''?> value="<?=$volume->id?>">≈ <?=$volume->volume?>ml</option>
                        <?php endforeach; ?>
                    </select>
                    <div class="invalid-feedback"><?=$data['volume_err']?></div>
                </div>
                <!-- État -->
                <div class="col">
                    <label for="etat">État</label>
                    <select class="custom-select <?=(!empty($data['etat_err'])) ? 'is-invalid' : ''?>" name="etat">
                        <option <?=($data['etat'] == '') ? 'selected' : ''?>  disabled>Choisissez l'état...</option>
                        <?php foreach($data['select_etats'] as $etat): ?>
                            <option <?=($data['etat'] == $etat->id) ? 'selected' : ''?> value="<?=$etat->id?>"><?=$etat->libelle?></option>
                        <?php endforeach; ?>
                    </select>
                    <div class="invalid-feedback"><?=$data['etat_err']?></div>
                </div>
            </div>
        </div>

        <!-- Photos -->
        <div class="form-group">
            <div class="form-row d-flex align-items-end">

                <!-- Photo 1 -->
                <div class="col mr-3">
                    <div class="row">
                        <label class="col-auto mr-auto" for="photo1">Photo 1 (principale)</label>
                        <small class="col-auto">Champ requis</small>
                    </div>
                    <div class="custom-file">
                        <input class="custom-file-input <?=(!empty($data['photo1_err'])) ? 'is-invalid' : ''?>" type="file" name="photo1" aria-describedby="photoHelp1">
                        <label class="custom-file-label" for="photo1" data-browse="Parcourir...">Choisissez une photo</label>
                        <div class="invalid-feedback"><?=$data['photo1_err']?></div>
                    </div>
                    <small class="form-text text-muted" id="photoHelp1">Fichiers acceptés : .png, .jpg, .jpeg</small>
                </div>

                <!-- Photo 2 -->
                <div class="col mr-3">
                    <div class="row">
                        <label class="col-auto mr-auto" for="photo2">Photo 2</label>
                    </div>
                    <div class="custom-file">
                        <input class="custom-file-input <?=(!empty($data['photo2_err'])) ? 'is-invalid' : ''?>" type="file" name="photo2" aria-describedby="photoHelp2">
                        <label class="custom-file-label" for="photo2" data-browse="Parcourir...">Choisissez une photo</label>
                        <div class="invalid-feedback"><?=$data['photo2_err']?></div>
                    </div>
                    <small class="form-text text-muted" id="photoHelp2">Fichiers acceptés : .png, .jpg, .jpeg</small>
                </div>

                <!-- Photo 3 -->
                <div class="col mr-3">
                    <div class="row">
                        <label class="col-auto mr-auto" for="photo3">Photo 3</label>
                    </div>
                    <div class="custom-file">
                        <input class="custom-file-input <?=(!empty($data['photo3_err'])) ? 'is-invalid' : ''?>" type="file" name="photo3" aria-describedby="photoHelp3">
                        <label class="custom-file-label" for="photo3" data-browse="Parcourir...">Choisissez une photo</label>
                        <div class="invalid-feedback"><?=$data['photo3_err']?></div>
                    </div>
                    <small class="form-text text-muted" id="photoHelp3">Fichiers acceptés : .png, .jpg, .jpeg</small>
                </div>
            </div>
        </div>

        <!-- <input class="btn btn-primary btn-block" type="submit" value="Créer et soumettre l'annonce à la modération"> -->
        <input class="btn btn-primary btn-block" type="submit" value="Déposer l'annonce">
    </form>
</div>

<?php require APPROOT . '/views/inc/footer.php';?>