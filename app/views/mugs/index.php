<?php 
    // Check if Mug exists and if it public
    if($data['mug']->id) {
        if($data['mug']->idStatut != 1 && $data['mug']->idVendeur != $_SESSION['user_id']) {
            redirect('');
        }
    } else {
        redirect('');
    }


    isset($data['mug']->id) && $data['mug']->idStatut == 1 ? true : redirect('');
    
    // Add all Mug's photos in a array
    $photosArray = [$data['mug']->photo1];
    $data['mug']->photo2 ? array_push($photosArray, $data['mug']->photo2) : false;
    $data['mug']->photo3 ? array_push($photosArray, $data['mug']->photo3) : false;
?>

<?php require APPROOT . '/views/inc/header.php';?>

<div class="row-cols-1">
    <div class="col-md-8 mx-auto">
        <!-- Titre -->
        <h1 class="text-center"><?=$data['mug']->titre?></h1>
        <h5 class="text-center text-muted"><span class="badge badge-primary"><?=$data['mug']->prixTtc?>€</span> TTC</h5>

        <!-- Photos -->
        <div class="row no-gutters mt-md-4" style="height: 15em;">
            <?php foreach($photosArray as $index => $photo):?>
                <div class="col bg-img <?=($index != count($photosArray)-1) ? 'mr-3' : ''?>" style="background-image: url('<?=URLROOT?>/img/mugs/<?=$photo?>');"></div>
            <?php endforeach;?>
        </div>

        <hr class="my-md-4">

        <!-- Critères -->
        <div class="row no-gutters">
            <h3 class="col-12 mb-3">Critères</h3>

            <div class="col">
                <h6 class="text-uppercase">Couleur</h6>
                <p class="text-muted"><?=$data['mug']->couleur?></p>
            </div>
            <div class="col">
                <h6 class="text-uppercase">Type</h6>
                <p class="text-muted"><?=$data['mug']->type?></p>
            </div>
            <div class="col">
                <h6 class="text-uppercase">Volume</h6>
                <p class="text-muted"><?=$data['mug']->volume?>ml</p>
            </div>
            <div class="col">
                <h6 class="text-uppercase">État</h6>
                <p class="text-muted"><?=$data['mug']->etat?></p>
            </div>
        </div>

        <hr class="my-md-4">

        <!-- Description -->
        <h3 class="col-12 px-0 mb-3">Description</h3>
        <p><?=$data['mug']->description?></p>

        <hr class="my-md-4">

        <!-- Informations -->
        <p>Mug publié le <?=date('d/m/Y \à H:i', strtotime($data['mug']->createdAt))?> par <a href="<?=URLROOT?>/users/<?=$data['user']->id?>"><?=$data['user']->pseudo?></a>.</p>

        <?php if(isset($_SESSION['user_id']) && $data['user']->id == $_SESSION['user_id']):?>
            <div class="row mt-4">
                <a class="col btn btn-secondary btn-block" href="<?=URLROOT?>/mugs/edit/<?=$data['mug']->id?>">Modifier votre annonce</a>
                <form class="col" action="<?=URLROOT?>/mugs/delete/<?=$data['mug']->id?>" method="post">
                    <button class="btn btn-danger btn-block" type="submit" value="Delete">Supprimer votre annonce</button>
                </form>
            </div>
        <?php else:?>
            <form class="mt-4" action="<?=URLROOT?>/mugs/<?=$data['mug']->id?>" method="post">
                <button class="btn btn-primary btn-block" name="add_panier" type="submit">Ajouter au panier</button>
            </form>
        <?php endif;?>
    </div>
</div>

<?php require APPROOT . '/views/inc/footer.php';?>