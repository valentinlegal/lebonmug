<?php require APPROOT . '/views/inc/header.php';?>

<div class="row-cols-1">
    <div class="col-md-8 mx-auto">

        <!-- Alert -->
        <div class="row">
            <div class="col-12">
                <?=flash('user_message')?>
                <?=flash('mug_message')?>
            </div>
        </div>

        <h1 class="text-center mb-md-5">Bienvenue sur LeBonMug !</h1>

        <!-- Recherche -->
        <!-- <h2>Trouvez le mug parfait !</h2>
        <form class="mx-auto mt-md-4" action="<?=URLROOT?>/" method="post">
            <div class="form-group">
                <div class="form-row">
                    <div class="col mr-3">
                        <label for="couleur">Couleur principale</label>
                        <select class="custom-select" name="couleur">
                            <option value="0" <?=($data['couleur'] == '') ? 'selected' : ''?>>Choisissez la couleur...</option>
                            <?php foreach($data['select_couleurs'] as $couleur): ?>
                                <option <?=($data['couleur'] == $couleur->id) ? 'selected' : ''?> value="<?=$couleur->id?>"><?=$couleur->libelle?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>

                    <div class="col">
                        <label for="type">Type</label>
                        <select class="custom-select" name="type">
                            <option value="0" <?=($data['type'] == '') ? 'selected' : ''?>>Choisissez le type...</option>
                            <?php foreach($data['select_types'] as $type): ?>
                                <option <?=($data['type'] == $type->id) ? 'selected' : ''?> value="<?=$type->id?>"><?=$type->libelle?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <div class="form-row">
                    <div class="col mr-3">
                        <label for="volume">Volume (approximatif)</label>
                        <select class="custom-select" name="volume">
                            <option value="0" <?=($data['volume'] == '') ? 'selected' : ''?>>Choisissez le volume...</option>
                            <?php foreach($data['select_volumes'] as $volume): ?>
                                <option <?=($data['volume'] == $volume->id) ? 'selected' : ''?> value="<?=$volume->id?>">≈ <?=$volume->volume?>ml</option>
                            <?php endforeach; ?>
                        </select>
                    </div>

                    <div class="col">
                        <label for="etat">État</label>
                        <select class="custom-select" name="etat">
                            <option value="0" <?=($data['etat'] == '') ? 'selected' : ''?>>Choisissez l'état...</option>
                            <?php foreach($data['select_etats'] as $etat): ?>
                                <option <?=($data['etat'] == $etat->id) ? 'selected' : ''?> value="<?=$etat->id?>"><?=$etat->libelle?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <div class="form-row">
                    <div class="col-6 form-row">
                        <div class="col mr-3">
                            <label for="prix_min">Prix entre</label>
                            <div class="input-group">
                                <input class="form-control <?=(!empty($data['prix_min_err'])) ? 'is-invalid' : ''?>" type="text" name="prix_min" placeholder="ex : 9" value="<?=$data['prix_min']?>">
                                <div class="input-group-append">
                                    <span class="input-group-text">,00 €</span>
                                </div>
                                <div class="invalid-feedback"><?=$data['prix_min_err']?></div>
                            </div>
                        </div>

                        <div class="col">
                            <label for="prix_max">et</label>
                            <div class="input-group">
                                <input class="form-control <?=(!empty($data['prix_max_err'])) ? 'is-invalid' : ''?>" type="text" name="prix_max" placeholder="ex : 17" value="<?=$data['prix_max']?>">
                                <div class="input-group-append">
                                    <span class="input-group-text">,00 €</span>
                                </div>
                                <div class="invalid-feedback"><?=$data['prix_max_err']?></div>
                            </div>
                        </div>
                    </div>

                    <div class="col pl-4">
                        <label for="date">Annonce publiée avant le</label>
                        <input class="form-control <?=(!empty($data['date_err'])) ? 'is-invalid' : ''?>" type="date" name="date" value="<?=$data['date']?>">
                        <div class="invalid-feedback"><?=$data['date_err']?></div>
                    </div>
                </div>
            </div>

            <input class="btn btn-primary btn-block" type="submit" value="Lancer la recherche">
        </form>

        <?php if(isset($data['res_search'])):?>
            <?php if($data['res_search']):?>
                <?php foreach($data['res_search'] as $mug): ?>
                    <?php if($mug->idStatut == 1): ?>
                        <div class="card my-4">
                            <div class="row no-gutters">
                                <div class="col-4 bg-img" style="background-image: url(<?=$mug->photo1?>);"></div>
                                <div class="col-8">
                                    <div class="card-body">
                                        <h5 class="card-title mb-2"><?=$mug->titre?></h5>
                                        <span class="badge badge-primary mb-2"><?=$mug->prixTtc?>€</span>
                                        <p class="card-text text-truncate"><?=$mug->description?></p>
                                        <p class="card-text"><small class="text-muted"><?=date('d/m/Y \à H:i', strtotime($mug->createdAt))?></small></p>
                                        <a class="btn btn-primary" href="<?=URLROOT?>/mugs/<?=$mug->id?>">En savoir plus</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>
                <?php endforeach; ?>
            <?php else: ?>
                <p class="mt-4 text-center font-italic text-muted">Aucun mug trouvé pour ces critères.</p>
            <?php endif; ?>
        <?php endif; ?> -->




        <?php if($data['mugs']):?>

        <!-- <hr class="my-md-4"> -->

        <!-- Derniers ajouts -->
        <div>
            <div class="row">
                <h2 class="col mr-auto">Les dernières stars...</h2>
                <form class="col-auto row" action="<?=URLROOT?>/" method="post">
                    <select class="col custom-select" name="tri">
                        <option value="plus_recents" <?=$data['selected_tri'] == 'plus_recents' ? 'selected' : ''?>>Tri : Plus récents</option>
                        <option value="plus_anciens" <?=$data['selected_tri'] == 'plus_anciens' ? 'selected' : ''?>>Tri : Plus anciens</option>
                        <option value="prix_croissants" <?=$data['selected_tri'] == 'prix_croissants' ? 'selected' : ''?>>Tri : Prix croissants</option>
                        <option value="prix_decroissants" <?=$data['selected_tri'] == 'prix_decroissants' ? 'selected' : ''?>>Tri : Prix décroissants</option>
                    </select>
                    <div class="col-auto"><input class="col btn btn-secondary" type="submit" value="Trier"></div>
                </form>
            </div>
            
            <div>
                <?php foreach($data['mugs'] as $mug): ?>
                    <?php if($mug->idStatut == 1): ?>
                        <div class="card my-4">
                            <div class="row no-gutters">
                                <div class="col-4 bg-img" style="background-image: url('<?=URLROOT?>/img/mugs/<?=$mug->photo1?>');"></div>
                                <div class="col-8">
                                    <div class="card-body">
                                        <h5 class="card-title mb-2"><?=$mug->titre?></h5>
                                        <span class="badge badge-primary mb-2"><?=$mug->prixTtc?>€</span>
                                        <p class="card-text text-truncate"><?=$mug->description?></p>
                                        <p class="card-text"><small class="text-muted"><?=date('d/m/Y \à H:i', strtotime($mug->createdAt))?></small></p>
                                        <a class="btn btn-primary" href="<?=URLROOT?>/mugs/<?=$mug->id?>">En savoir plus</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>
                <?php endforeach; ?>

                <!-- <nav aria-label="Page navigation">
                    <ul class="pagination justify-content-center">
                        <li class="page-item disabled"><a class="page-link" href="#" tabindex="-1" aria-disabled="true">&laquo;</a></li>
                        <li class="page-item active"><a class="page-link" href="#">1</a></li>
                        <li class="page-item"><a class="page-link" href="#">2</a></li>
                        <li class="page-item"><a class="page-link" href="#">3</a></li>
                        <li class="page-item"><a class="page-link" href="#">&raquo;</a></li>
                    </ul>
                </nav> -->
            </div>
            <?php else: ?>
                <p class="mt-4 text-center font-italic text-muted">Aucun mug n'est disponible à la vente pour le moment.</p>
            <?php endif;?>

        </div>
    </div>
    
</div>

<?php require APPROOT . '/views/inc/footer.php';?>