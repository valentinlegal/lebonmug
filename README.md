# LeBonMug

Création d'un site web basé sur un framework PHP (& MySQL) en MVC construit "from scratch". Il s'agit d'un site e-commerce (fictif) de vente et d'achat de mugs.

![LeBonMug cover](./public/img/cover.png)

## À propos du projet

- **Nature :** projet personnel
- **Date :** février 2020
- **Description :** il s'agit d'un site web construit à partir d'un framework PHP & MySQL en MVC. Ce framework a lui même été développé "from scratch". Le site représente un site (LeBonMug) d'e-commerce où l'on peut vendre ou acheter des mugs entre particuliers. Ce site est un concept, il s'agit d'annonces fictives et la simulation s'arrête avant le paiement

## Technologies utilisées

- PHP
- MySQL
- Bootstrap
- POO & MVC
- HTML
- CSS

## Informations complémentaires

- Vous pourrez retrouver la BDD du projet dans le fichier `lebonmug.sql` situé dans le répertoire racine
- Pensez à adapter les fichiers `/app/config/config.php` et `/public/.htaccess`

## Versions

- **1.0.0 :**
    - **Fonctionnalités :** un utilisateur peut se créer un compte et s'y connecter. Il peut aussi déposer une annonce sur site, ou même réserver un mug (en le mettant dans son panier) et l'acheter (fictivement) par la suite. L'utilisateur peut modifier son compte après création. Il en va de même pour les annonces qu'il a créé (CRUD)
    - **Temps de réalisation :** environ 1 semaine